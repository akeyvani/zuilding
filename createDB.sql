﻿create table InitialProjectDefinitions
(
	Id integer primary key,
	landArea decimal, 
	landFSR decimal,
	landPrice decimal,
	grossResidentialAea decimal,
	grossCommercialArea decimal,
	residentialSellableArea decimal,
	commercialSellableArea  decimal,
	numResidentialUnits int,
	numCommercialUnits int,	
	numParkingRequired int,
	numParkingProposed int	
)	