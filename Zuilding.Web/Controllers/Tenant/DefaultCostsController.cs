﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Zuilding.Web.Domain;
using Zuilding.Web.Domain.Tenant;
using Zuilding.Web.Repository;

namespace Zuilding.Web.Controllers
{
    public class DefaultCostsController : TenantDataRESTController<Cost>
    {
        // MOVE: somewhere
        public DefaultCostsController()
        {
            // init
            if (_repository.getAll().Count() == 0)
                foreach (var c in Proposal.buildDefaultCosts())
                    _repository.create(c);
        }
    }
}