﻿using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using Zuilding.Web.Domain;
using Zuilding.Web.Repository;


namespace Zuilding.Web.Controllers
{

    //public class ProposalFilesActionFilterAttribute: ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
    //    {
    //        const string PID_KEY = "pid";
    //        object proposalId;

    //        if (actionContext.RequestContext.RouteData.Values.TryGetValue(PID_KEY, out proposalId))
    //        {
    //            var s = proposalId.ToString();
    //        }

    //        base.OnActionExecuting(actionContext);
    //    }
    //}


    //[ProposalFilesActionFilterAttribute]
    [RoutePrefix("api/files")]
    public class FilesController: TenantDataRESTController<FileTag>
    {
        protected new FileRepository _repository { get { return base._repository as FileRepository; } }
        
        protected override TenantRepository<FileTag> _factoryCreateRepository()
        {
            return new FileRepository(_tenantId);
        }

        
        // download files through here. default get returns the file tag.
        public HttpResponseMessage get(string id, bool getData)
        {
            string type;
            var c = new StreamContent(_repository.getFileData(id, out type));
            c.Headers.ContentType = new MediaTypeHeaderValue(type);

            var resp = new HttpResponseMessage(HttpStatusCode.OK) { Content = c };
            return resp;
        }

        // upload files through here
        public async Task<FileTag> post()
        {
            if (!Request.Content.IsMimeMultipartContent())
                throw new Exception(); // divided by zero

            // read file from request
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            var file = provider.Contents[0];

            // get filename
            var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
            var fileType = file.Headers.ContentType.MediaType;

            //
            var fileTag = (_repository as FileRepository).create(
                filename,
                fileType,
                await file.ReadAsStreamAsync(),
                _getUserAccount(User.Identity.Name).Id);

            return fileTag;
        }

        // disable default post implementation
        [Route(Order = 2)]
        public override string post(FileTag o)
        {
            throw new HttpRequestException("not implemented");
        }

        public override void put(string id, FileTag o)
        {
            base.patch(id, o);
        }
    }

}