﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Zuilding.Web.Repository;
using System.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using Zuilding.Web.Domain;

using MongoDB.AspNet.Identity;

namespace Zuilding.Web.Controllers
{
    // basic REST endpoint for CRUD + index of objects of type T for logged in tenant
    public class TenantDataRESTController<T> : ApiController where T : ITenantEntity 
    {
        // instantiation - plug in the repository to user or use default
        protected readonly TenantRepository<T> _repository;
        protected readonly string _tenantId;

        protected static Account _getUserAccount(string userName)
        {
            var repo = AccountsRepository.FactoryCreate();
            var acc = repo.getByName(userName);
            return acc;
        }

        protected virtual TenantRepository<T> _factoryCreateRepository()
        {
            return new TenantRepository<T>(_tenantId);
        } 
        public TenantDataRESTController(string tenantId = null)
        {
            _tenantId = tenantId ?? _getUserAccount(User.Identity.Name).tenantId; // use current logged in user's identity
            _repository = _factoryCreateRepository();
        }

        public virtual T get(string id)
        {
            return _repository.read(id);
        }
        public virtual IEnumerable<T> getAll()
        {
            return _repository.getAll();
        }
        public virtual void put(string id, T o)
        {
            _repository.update(id, o, false);
        }
        public virtual string post(T o)
        {
            return _repository.create(o);
        }
        public virtual void delete(string id)
        {
            _repository.delete(id);
        }

        // make partial updates. yes?
        public virtual void patch(string id, T o)
        {
            //throw new NotImplementedException();
            _repository.update(id, o, true);
        }

    }
}