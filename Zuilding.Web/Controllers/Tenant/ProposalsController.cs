﻿using Microsoft.AspNet.Identity;
using MongoDB.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Zuilding.Web.Domain;
using Zuilding.Web.Domain.Tenant;
using Zuilding.Web.Repository;


namespace Zuilding.Web.Controllers
{
    public class ProposalsController : TenantDataRESTController<Proposal>
    {
        public override string post(Proposal o)
        {
            o.dateModified = o.dateCreated = DateTime.Now;
            return base.post(o);
        }

        public override void put(string id, Proposal o)
        {
            o.dateModified = DateTime.Now;
            base.patch(id, o);
        }

        [HttpGet]
        [Route("~/api/proposalHeaders")]
        public IEnumerable<ProposalHeader> getAllHeaders()
        {
            return _repository.getAll().Select(p => _createHeader(p)).ToList();
        }

        [HttpGet]
        [Route("api/proposalHeaders/{id}")]
        public ProposalHeader getHeader(string id)
        {
            return _createHeader(_repository.read(id));
        }
        protected ProposalHeader _createHeader(Proposal p)
        {
            return new ProposalHeader
            {
                id = p.id,
                name = p.name,
                dateCreated = p.dateCreated,
                dateModified = p.dateModified,
            };
        }

        [HttpDelete]
        [Route("api/proposalHeaders/{id}")]
        public override void delete(string id)
        {
            base.delete(id);
        }
    }
}