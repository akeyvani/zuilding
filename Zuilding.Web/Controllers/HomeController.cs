﻿using Microsoft.AspNet.Identity;
using MongoDB.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Zuilding.Web.Domain;
using Zuilding.Web.Repository;

namespace Zuilding.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
                if (User.IsInRole("Builder"))
                    return View("/Views/builder.cshtml");
            return View("/Views/login.cshtml");
        }
    }
}
