﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Web.Security;
using Zuilding.Web.Domain;
using Zuilding.Web.Repository;
using System.Security.Claims;

namespace Zuilding.Web.Controllers
{
    // TODO authentication
    public class AccountsController : ApiController
    {
        private readonly AccountsRepository _accountsRep;

        public AccountsController()
        {
            var roles = ((ClaimsIdentity)User.Identity).Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .Select(c => c.Value);

            // create account repository based on user role
            _accountsRep = AccountsRepository.FactoryCreate(roles);
        }

        // get account info for current user
        public Account get()
        {
            return _accountsRep.getByName(User.Identity.Name);
        }

        // get all accounts for a given tenant
        public IEnumerable<Account> getAll(string id)
        {
            // TODO: refactor
            // can only get accounts for logged in tenant
            var acc = _accountsRep.getByName(User.Identity.Name);
            if (acc.tenantId != id)
                throw new Exception("not authroized");

            // only tenant admin users can do this
            if (!acc.roles.Contains("TenantAdmin")) // TODO: use enum
                throw new Exception("not authorized");

            return _accountsRep.getByTenantId(id);
        }

        // TODO: authorize roles
        // create new user 
        public string post(Account user)
        {
            // set tenantid to current tenant TODO: attack?
            var acc = _accountsRep.getByName(User.Identity.Name);
            user.tenantId = acc.tenantId;

            // create the account
            return _accountsRep.create(user, "password");
            // TODO: send email with username and random password
        } 

        // update account info for current user
        public void put(Account user)
        {
            if (_accountsRep.getByName(User.Identity.Name).tenantId != user.tenantId)
                throw new Exception("not authorized");
            
            _accountsRep.update(user.Id, user, false);
        }

        public void delete(string id)
        {
            // only tenant admin users can do this
            var acc = _accountsRep.get(id);
            if (!acc.roles.Contains("TenantAdmin")  || acc.UserName == User.Identity.Name) // TODO: use enum
                throw new Exception("not authorized");

            // TODO: AUTHORIZATION!
            _accountsRep.delete(id);
        }
    }
}