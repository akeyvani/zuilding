﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using MongoDB.Driver.GridFS;

namespace Zuilding.Web.Controllers
{
    public class UploadController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> Post()
        {
            if (!Request.Content.IsMimeMultipartContent())
                throw new Exception(); // divided by zero

            // read file from request
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            var file = provider.Contents[0];

            // get filename
            var filename = file.Headers.ContentDisposition.FileName.Trim('\"');

            ////
            //var id = (_repository as FileRepository).create(
            //    filename,
            //    await file.ReadAsStreamAsync(),
            //    _getUserAccount(User.Identity.Name).Id);

            return Ok<string>("swfewefwefwe");
        }
    }
}
