﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Zuilding.Web.Domain;

namespace Zuilding.Web.Infrastructure
{ 
    public class AccountsDbContext : DbContext
    {
        public AccountsDbContext(): base("DefaultConnection")
        {
        }

        public DbSet<Account> accounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Configurations.Add(new ZuildingDbConfiguration());
            
            base.OnModelCreating(modelBuilder);
        }
    }
}