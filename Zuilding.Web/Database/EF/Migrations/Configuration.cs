namespace Zuilding.Web.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Security;
    using WebMatrix.WebData;
    using Zuilding.Web.Domain;
    using Zuilding.Web.Infrastructure;
    using Zuilding.Web.Repository;

    internal sealed class Configuration : DbMigrationsConfiguration<AccountsDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        // define roles
        private enum TenantRole
        {
            Builder,
            Investor,
            Administrator,
            TenantAdmin
        }

        // define test accounts
        private static readonly IDictionary<TenantRole, Account> TestAccounts = new Dictionary<TenantRole, Account>
        {
            {TenantRole.Builder, new Account { name = "builder", tenantId = "test"}},
            {TenantRole.Investor, new Account { name = "investor", tenantId = "test-inv"}},
            {TenantRole.Administrator, new Account { name = "admin", tenantId = ""}}
        };

        protected void initMembership(AccountsDbContext context)
        {
            // already initialized if admin already there
            if (WebSecurity.UserExists(TestAccounts[TenantRole.Administrator].name))
                return;

            // create roles
            var allRoles = Enum.GetNames(typeof(TenantRole));
            foreach (var r in allRoles)
                Roles.CreateRole(r);

            // add the users
            foreach (var i in TestAccounts)
            {
                var acc = i.Value;
                var role = i.Key.ToString();

                // supply tenant role. the accounts repository will add other roles as needed.
                acc.roles = new Collection<string> { role };

                // create account repository based on tenant role
                var rep = AccountsRepository.FactoryCreate(acc.roles);

                // create tenant and account
                rep.create(acc);
            }
        }

        protected override void Seed(AccountsDbContext context)
        {
            // connect to sql db
            WebSecurity.InitializeDatabaseConnection(
                   "DefaultConnection",
                   "Accounts",
                   "userId",
                   "name", autoCreateTables: true);

            // create users
            initMembership(context);
        }
    }
}
