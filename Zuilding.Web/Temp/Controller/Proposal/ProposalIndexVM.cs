﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Zuilding.Web.Domain;

namespace Zuilding.Web.ViewModels
{
    public class ProposalIndexVM
    {
        public ProposalIndexVM(Proposal p)
        {
            Mapper.CreateMap<Proposal, ProposalIndexVM>();
            Mapper.Map<Proposal, ProposalIndexVM>(p, this);
        }
        
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Project Name")]
        [MaxLength(80)]
        public string Name { get; set; }

        [Required]
        [Display( Name = "Date Modified")]
        [DataType(DataType.Date)]
        public DateTime dateModified { get; set; }

        //
        // ...other info
    }
}
