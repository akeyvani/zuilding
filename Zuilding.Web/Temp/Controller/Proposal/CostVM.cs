﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zuilding.Web.Domain;

namespace Zuilding.Web.ViewModels
{
    public class CostVM
    {
        public CostVM() { }
        public CostVM(Cost c)
        {
            Mapper.CreateMap<Cost, CostVM>();
            Mapper.Map<Cost, CostVM>(c, this);
        }

        public Cost export(Cost c)
        {
            Mapper.CreateMap<CostVM, Cost>();
            Mapper.Map<CostVM, Cost>(this, c);

            return c;
        }

        public int Id { get; set; }
        public string name { get; set; }
        public string groupName { get; set; }
        public decimal amount { get; set; }
        public decimal? defaultAmount { get; set; }
        public bool isPercentage { get; set; }
        public decimal? multiplier { get; set; }
        public bool isOptional { get; set; }
        public bool isOptionalActive { get; set; }

    }

    public class HardCostVM: CostVM
    {
        public HardCostVM() { }
        public HardCostVM(HardCost c): base(c)
        {
            buildableArea = c.buildableArea.Clone();
        }

        public Cost export(HardCost c)
        {
            base.export(c);
            c.buildableArea = buildableArea.Clone();
            return c;
        }

        public BuildableArea buildableArea { get; set; }
    }
}
