﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Zuilding.Web.Domain;

namespace Zuilding.Web.Services.Project
{
    public class BuildableAreaVM
    {
        public BuildableAreaVM(BuildableArea ba)
        {
            Mapper.CreateMap<BuildableArea, BuildableAreaVM>();
            Mapper.Map<BuildableArea, BuildableAreaVM>(ba, this);
        }

        BuildableArea export( BuildableArea baOut )
        {
            Mapper.CreateMap<BuildableAreaVM, BuildableArea>();
            Mapper.Map<BuildableAreaVM, BuildableArea>(this, baOut);

            return baOut;
        }

        public int Id { get; set; }
        public string name { get; set; }
        public decimal area { get; set; }
        public decimal percentSellable { get; set; }
        public decimal salePrice { get; set; }
        public int numUnits { get; set; }
        public decimal totalValue() { return area * percentSellable * salePrice; }
    }
}