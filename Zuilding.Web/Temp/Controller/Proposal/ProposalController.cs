﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using Zuilding.Web.Domain;
using Zuilding.Web.Infrastructure;
using Zuilding.Web.Models;
using Zuilding.Web.ViewModels;

namespace Zuilding.Web.Controllers
{
    public class ProposalController : Controller
    {
        [Authorize(Roles = "Builder,Investor")]
        [HttpGet]
        public ActionResult Index()
        {
            var db = new ZuildingDb();
            var user = currentUser(db);

            if (user is Builder)
            {
                // read current builder from db
                var aBuilder = new BuilderAggregate(db, user.UserId);
                aBuilder.read();

                // create vm
                var indexvm = aBuilder._root.proposals.Select(p => new ProposalIndexVM(p));

                return View(indexvm);
            }
            else
            {
                // read current investor from db
                var aInvestor = new InvestorAggregate(db, user.UserId);
                aInvestor.read();

                // create vm 
                var indexvm = aInvestor.proposals().Select(p=> new ProposalIndexVM(p));

                return View(indexvm);
            }
        }

        [HttpGet]
        [Authorize(Roles = "Builder,Investor")]
        public ActionResult Details(int id)
        {
            // read from db
            var pa = new ProposalAggregate( new ZuildingDb(), id);
            pa.read();

            return View(new ProposalVM(pa._root));
        }

        [HttpGet]
        [Authorize(Roles = "Builder")]
        public ActionResult Edit(int id)
        {
            // read from db
            var db = new ZuildingDb();
            var pa = new ProposalAggregate(db, id);
            pa.read();

            // make sure the belongs to the this builder
            assertBuilderContainsProposal(db, pa._root);

            // create viewmodel and view
            return View(new ProposalVM(pa._root));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProposalVM vmprop)
        {
            // create proposal from vm
            var proposal = Proposal.factoryCreateProposal();
            vmprop.export(proposal);

            // time stamp it
            proposal.timestampModified();

            // read old proposal from db
            using (var pa = new ProposalAggregate(new ZuildingDb(), proposal.Id))
            {
                // read
                pa.read();

                // update
                pa.update(proposal);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Builder")]
        public ActionResult Create()
        {
            // create blank proposal
            var p = Proposal.factoryCreateProposal();
            p.costs = Proposal.buildDefaultCosts();

            return View(new ProposalVM(p));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProposalVM propvm)
        {
            if (!ModelState.IsValid)
                return View();

            // create proporsal from vm
            var proposal = Proposal.factoryCreateProposal();
            propvm.export(proposal);

            proposal.timestampCreated();

            // add to db
            var db = new ZuildingDb();
            var aProposal = new ProposalAggregate(db, proposal);
            aProposal.create();

            // add to current builder
            var aBuilder = new BuilderAggregate(db, currentUser(db) as Builder);
            aBuilder.read();
            aBuilder._root.proposals.Add(aProposal._root);
            aProposal.commit();
            
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Builder")]
        public ActionResult Delete(int id)
        {
            var db = new ZuildingDb();

            // read proposal
            var aProposal = new ProposalAggregate(db, id);

            // makes sure the proposal belongs to this builder
            assertBuilderContainsProposal(db, aProposal._root);

            // delete
            aProposal.delete();
            aProposal.commit();

            return RedirectToAction("Index");
        }

        private void assertBuilderContainsProposal(ZuildingDb db, Proposal proposal)
        {
            // read builder
            var aBuilder = new BuilderAggregate(db, currentUser(db) as Builder);
            aBuilder.read();

            // make sure builder does contain this actual proposal
            if (!aBuilder._root.proposals.Contains(proposal))
                throw new HttpRequestValidationException();
        }
        private User currentUser(ZuildingDb db)
        {
            return db.SystemUsers.First(e => e.UserId == (int)WebSecurity.CurrentUserId) as User;
        }
}
}
