﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Zuilding.Web.Domain;

namespace Zuilding.Web.ViewModels
{
    public class CostGroupVM
    {
        public CostGroupVM() { costs = new Collection<CostVM>(); inactiveCosts = new Collection<CostVM>(); }

        public ICollection<Cost> export(ICollection<Cost> outCosts)
        {
            // Create a Cost for each CostVM object
            costs.Each(a => outCosts.Add(a.export(new Cost())));
            inactiveCosts.Each(a => outCosts.Add(a.export(new Cost())));

            return outCosts;
        }

        public CostGroupVM(string title, ICollection<Cost> activeCosts, ICollection<Cost> inactiveCosts)
        {
            this.name = title;
            this.costs = activeCosts.Select(c => new CostVM(c)).ToList();
            this.inactiveCosts = inactiveCosts.Select(c => new CostVM(c)).ToList();
        }

        public string name { get; set; }
        public decimal total() { return costs.Sum(a => a.amount); }
        public ICollection<CostVM> costs { get; set; }
        public ICollection<CostVM> inactiveCosts { get; set; }
    }

}