﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Zuilding.Web.Domain;

namespace Zuilding.Web.ViewModels
{
    public class ProposalVM
    {
        public ProposalVM() { }
        public ProposalVM(Proposal proposal)
        {
            Mapper.CreateMap<Proposal, ProposalVM>().ForMember(a => a.hardCosts, c => c.Ignore())
                                                    .ForMember(a => a.softCosts, c => c.Ignore())
                                                    .ForMember(a => a.landAcCosts, c => c.Ignore());
            Mapper.Map<Proposal, ProposalVM>(proposal, this);

            // hard costs
            hardCosts = proposal.hardCosts().Select(c => new HardCostVM(c)).ToList();
            
            // soft costs
            softCosts = new CostGroupVM("Soft Costs", proposal.costGroup("soft"), proposal.costGroup("soft", false));
            
            // land costs
            landAcCosts = new CostGroupVM("Land Acquisition Costs", proposal.costGroup("landAc"), proposal.costGroup("landAc", false));
            landPrice = proposal.costGroup("land").First(a => a.name == "landprice").amount;

            // management costs
            manageFeeCon = new CostVM(proposal.costGroup("management").First(a=>a.name=="construction"));
            manageFeeDev = new CostVM(proposal.costGroup("management").First(a=>a.name=="development"));
            manageFeeMS = new CostVM(proposal.costGroup("management").First(a => a.name == "marketing"));
            
            // contingency
            contingHard = new CostVM(proposal.costGroup("contingency").First(a => a.name == "hard"));
            contingSoft= new CostVM(proposal.costGroup("contingency").First(a => a.name == "soft"));
        }

        public Proposal export(Proposal p)
        {
            Mapper.CreateMap<ProposalVM, Proposal>().ForSourceMember(a => a.hardCosts, c => c.Ignore())
                                                    .ForSourceMember(a => a.softCosts, c => c.Ignore())
                                                    .ForSourceMember(a => a.landAcCosts, c => c.Ignore());

            // Create new Proposal and map fields
            Mapper.Map<ProposalVM, Proposal>(this, p);
            
            // 
            // hard
            hardCosts.Each(a => p.costs.Add( a.export( new HardCost() )));
            
            // soft
            softCosts.export(p.costs);
            
            // land 
            p.costs.Add(new Cost { amount = landPrice, groupName = "land", name = "landprice" });
            landAcCosts.export(p.costs);
                
            // management
            p.costs.Add(manageFeeCon.export( new Cost() ));
            p.costs.Add(manageFeeDev.export(new Cost()));
            p.costs.Add(manageFeeMS.export(new Cost()));
            
            // contingency
            p.costs.Add(contingHard.export(new Cost()));
            p.costs.Add(contingSoft.export(new Cost()));
            
            return p;
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(80)]
        [Display(Name = "Project Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string address { get; set; }

        [HiddenInput()]
        [Display(Name = "Date Created")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime dateCreated { get; set; }

        [Required]
        [Display(Name = "Date Modified")]
        [DataType(DataType.Date)]
        public DateTime dateModified { get; set; }

        
        [Display(Name = "Land Area")]
        public decimal landArea { get; set; }
        [Display(Name = "Land FSR")]
        public decimal landFSR { get; set; }

        // Building spec
        [Display(Name="Required Equity")]
        public decimal requiredEquity { get; set; }
        [Display(Name = "Project Duration")]
        public int projectDuration { get; set; }
            
        [Display(Name="Land Price")]
        public decimal landPrice { get; set; }
        // costs
        public ICollection<HardCostVM> hardCosts { get; set; }
        public CostGroupVM softCosts { get; set; }
        public CostGroupVM landAcCosts { get; set; }
        public CostVM manageFeeDev { get; set; }
        public CostVM manageFeeCon { get; set; }
        public CostVM manageFeeMS { get; set; }
        public CostVM contingHard { get; set; }
        public CostVM contingSoft { get; set; }
    }
}




/*
        [Display(Name = "Gross Residential Area")]
        public decimal grossResidentialArea { get; set; }
        [Display(Name = "Gross Commercial Area")]
        public decimal grossCommercialArea { get; set; }
        [Display(Name = "Residential Units")]
        public int numResidentialUnits { get; set; }
        [Display(Name = "Commercial Units")]
        public int numCommercialUnits { get; set; }
        [Display(Name = "Required Parking")]
        public int numParkingRequired { get; set; }
        [Display(Name = "Proposed Parking")]
        public int numParkingProposed { get; set; }
        [Display(Name = "Residential Sellable Area")]
        public decimal residentialSellableArea { get; set; }
        [Display(Name = "Commercial Sellable Area")]
        public decimal commercialSellableArea { get; set; }
        [Display(Name = "Residential Sale Price")]
*/