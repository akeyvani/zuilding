﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zuilding.Web.Repository
{
    public interface ITenantEntity
    {
        //[BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        string id { get; set; }
    }
}
