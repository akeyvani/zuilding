﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zuilding.Web.Domain;
using MongoDB.Driver.Linq;
using MongoDB.Driver.Builders;
using Zuilding.Web.Domain.Tenant;

namespace Zuilding.Web.Repository
{
    public class ProposalRepository: TenantRepository<Proposal>
    {
        public ProposalRepository(string tenantId): base(tenantId)
        {
        }

        //// remove all references to given profileId
        //public void purgeProfileId(string id)
        //{
        //    _collection.Update(Query<Proposal>.EQ(a => a.portfolioId, id), Update<Proposal>.Unset(a => a.portfolioId));
        //}
    }
}
