﻿

using System.IO;
using System.Threading.Tasks;
using Zuilding.Web.Domain;
using Zuilding.Web.Repository;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Builders;
using System;
using MongoDB.Bson;


namespace Zuilding.Web.Repository
{

    public class FileRepository: TenantRepository<FileTag> 
    {
        //protected readonly string _tenantId;
        protected readonly MongoGridFS _gridFS;

        public FileRepository(string tenantId): base(tenantId)
        {
            // get database - db name computed as below
            _gridFS = _db.GridFS;
        }

    
        // duplicate existing entry
        public override string create(FileTag f)
        {
            //
            // DO: increase ref count on f
            //

            return f.id;
        }

        // alternative create interface
        public FileTag create(string fileName, string fileType, Stream inputStream, string uploaderUserId)
        {
            var n = _gridFS.Upload(inputStream, fileName, new MongoGridFSCreateOptions { ContentType = fileType });
            
            // upload into gridFs and get id
            var fileId = n.Id.AsObjectId.ToString();

            // now create the tag with the id
            var file = new FileTag { fileName = fileName, type = fileType, uploaderId = uploaderUserId, fileId = fileId, isInternal = true };
            file.id = base.create(file);
            
            // return the created filetag
            return file;
        }

        public Stream getFileData(string id, out string type )
        {
            var file = _gridFS.FindOneById(new ObjectId(id));
            type = file.ContentType;

            return file.OpenRead();
        }

        public override void update(string id, FileTag entity, bool patch)
        {
            base.update(id, entity, patch);
        }

        public override void delete(string id)
        {
            // get the gridFS fileid first
            var fileId = base.read(id).fileId;
        
            // delete tag
            base.delete(id);

            //
            // TODO: if there are other fileTags with this fileId, don't remove the actual file
            //

            // delete actual file
            _gridFS.DeleteById(new ObjectId(fileId));
        }
    }
}