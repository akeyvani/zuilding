﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zuilding.Web.Repository
{
    public interface IRepository<T, TId>
    {
        TId create(T newEntity);
        T read(TId id);
        void update(TId id, T entity, bool patch);
        void delete(TId id);
        IQueryable<T> getAll();
    }
}
