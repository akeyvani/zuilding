﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
//using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Zuilding.Web.Repository
{
    // note: assumes _id is a propoerty of T
    // basic CRUD + index functionality on Mongo for entity type T 
    public class TenantRepository<T> : IRepository<T, string>
        where T : ITenantEntity
    {
        public static readonly string _connectionString = "mongodb://localhost";
        protected readonly string _tenantId;

        public readonly MongoDatabase _db;
        public readonly MongoCollection<T> _collection;

        public TenantRepository(string id, string dbName = null, string colName = null)
        {
            //var accRepo = AccountsRepository.FactoryCreate();

            // get database - db name computed as below
            _db = (new MongoClient(_connectionString)).GetServer().GetDatabase(dbName ?? "tenantdb-" + id);

            // use type name as collection name
            _collection = _db.GetCollection<T>(colName ?? typeof(T).Name);

            // store id of repository user
            _tenantId = id;
        }

        public virtual string create(T newEntity)
        {
            var res = _collection.Insert(newEntity);
            if (!res.Ok)
                throw new Exception("DB create error.");

            // return the _id field of the added object
            return newEntity.id;
        }

        public virtual T read(string id)
        {
            // returns null if not found
            return _collection.AsQueryable().FirstOrDefault(e => e.id == id);
        }

        public virtual void update(string id, T entity, bool patch)
        {
            // update non-null fields (REST patch)
            if (patch)
            {
                // convert to bson document with no id field
                entity.id = null;
                var doc = entity.ToBsonDocument();
                // update whatever fields are inside the bson doc
                _collection.Update(Query<T>.EQ(a => a.id, id), new UpdateDocument("$set", doc));
            }
            // replace with new entity (REST put)
            else
            {
                entity.id = id;
                _collection.Save(entity);
            }
        }

        public virtual void delete(string id)
        {
            var query = Query<T>.EQ(e => e.id, id);
            var res = _collection.Remove(query);
            if (!res.Ok)
                throw new Exception("DB delete error.");
        }

        public virtual IQueryable<T> getAll()
        {
            return _collection.FindAll().AsQueryable();
        }
    }
}