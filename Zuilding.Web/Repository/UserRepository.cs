﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Zuilding.Web.Domain;
using Zuilding.Web.EntityFramework;

namespace Zuilding.Web.Repository
{
    public class UserRepository
    {
        public readonly ipmEntities _db = new ipmEntities();
        public readonly int _userId;

        public UserRepository(int userid)
        {
            _userId = userid;
        }

        public IEnumerable<Contact> getContacts()
        {
            // get contacts for user with id: _userId
            return null;
        }

        public void createContact(int otherUserId)
        {
        }

        public void removeContact(int otherUserId)
        {
        }

        public void updateContact(Contact c)
        {
        }
    }
}