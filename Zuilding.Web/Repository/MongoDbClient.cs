﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zuilding.Web.Repository
{
    public class MongoDbClient
    {
        protected readonly string _connectionString = "mongodb://localhost";
        protected readonly MongoDatabase _db;

        public MongoDbClient(int userid) 
        {
            _db = (new MongoClient(_connectionString)).GetServer().GetDatabase( "userdb-" + userid );
        }
    }
}