﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Zuilding.Web.Domain.GooglePlaces;

namespace Zuilding.Web.Repository
{
    public class GooglePlacesRepository
    {
        public Dictionary<string, string> PlacesTypes { get; set; }

        public GooglePlacesRepository()
        {

            PlacesTypes = new Dictionary<string, string>();
            PlacesTypes.Add("food", "restaurant|food|bar|cafe");
            PlacesTypes.Add("school", "school|university");
            PlacesTypes.Add("culture", "art_gallery|amusement_park|museum|park|zoo|library");
            PlacesTypes.Add("city", "bank|city_hall|police|post_office|fire_station|local_government_office|doctor|dentist");
            PlacesTypes.Add("entertainment", "casino|night_club|bowling_alley|stadium|movie_theater");
            PlacesTypes.Add("shopping", "clothing_store|convenience_store|department_store|shoe_store|shopping_mall|grocery_or_supermarket|home_goods_store|hardware_store|book_store");
            PlacesTypes.Add("transportation", "bus_station|subway_station|train_station");
        }

        public MapPlacesViewModel getNearbyPlaces(double lat, double lng)
        {
            var result = new MapPlacesViewModel()
            {
                lat = lat,
                lng = lng
            };

            result.places = new List<PlaceViewModel>();

            foreach (var type in PlacesTypes)
            {
                addPlaces(result, type.Value, type.Key);
            }
            return result;
        }

        private void addPlaces(MapPlacesViewModel model, string types, string label)
        {
            StringBuilder address = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
            
            //******************** NEED GOOGLE KEY *****************/
            //******************** NEED GOOGLE KEY *****************/
            //******************** NEED GOOGLE KEY *****************/
            //******************** NEED GOOGLE KEY *****************/
            //******************** NEED GOOGLE KEY *****************/
            var GooglePlacesKey = "xxxxxx"; // get from web config
            address.Append("&key=" + GooglePlacesKey);
            address.Append("&location=" + model.lat.ToString() + "," + model.lng.ToString());
            address.Append("&radius=1500");//address.Append("&rankby=distance");
            address.Append("&sensor=false");
            address.Append("&types=" + types);

            try
            {
                HttpClient client = new HttpClient();
                var responseContent = client.GetAsync(address.ToString()).Result;
                var reponseString = responseContent.Content.ReadAsStringAsync().Result;
                PlacesResponse placesNearBy = JsonConvert.DeserializeObject<PlacesResponse>(reponseString);
                foreach (var place in placesNearBy.results)
                {
                    model.places.Add(new PlaceViewModel()
                    {
                        type = label,
                        lat = place.geometry.location.lat,
                        lng = place.geometry.location.lng,
                        name = place.name,
                        vicinity = place.vicinity
                    });
                }
            }
            catch
            {
            }
        }

    }
}