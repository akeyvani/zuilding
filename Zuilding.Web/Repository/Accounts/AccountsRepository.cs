﻿using Microsoft.AspNet.Identity;
using MongoDB.AspNet.Identity;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Zuilding.Web.Domain;
using MongoDB.Driver.Linq;


namespace Zuilding.Web.Repository
{
    // NOTE: authorization is responsibality of client class
    public class AccountsRepository : TenantRepository<Account>
    {
        public readonly UserStore<Account> _userStore;
        public readonly UserManager<Account> _manager;

        // instantiate an accounts repository based on the role of the user
        static public AccountsRepository FactoryCreate(IEnumerable<string> roles = null)
        {
            //if (roles != null)
            //{
            //    if (roles.Contains("Builder"))
            //        return new BuilderRepository();
            //    else if (roles.Contains("Investor"))
            //        return new InvestorRepository();
            //}

            return new BuilderRepository();
        }

        protected AccountsRepository()
            : base(null, "authentication", "AspNetUsers")
        {
            _userStore = new UserStore<Account>("mongodb://localhost/authentication");
            _manager = new UserManager<Account>(_userStore);
        }

        public Account get(string id)
        {
            return _manager.FindById(id);
        }

        public Account getByEmail(string email)
        {
            return _collection.AsQueryable().FirstOrDefault(a => a.email == email);
        }

        public Account getByName(string name)
        {
            return _manager.FindByName(name);
        }

        public IEnumerable<Account> getByTenantId(string id)
        {
            return _collection.AsQueryable().Where(a => a.tenantId == id);
        }

        public bool tenantExists(string id)
        {
            return _collection.AsQueryable().Any(a => a.tenantId == id);
        }
        public override void update(string id, Account account, bool patch)
        {
            // update roles
            var currentRoles = _manager.FindById(account.Id).Roles;
            foreach (var r in account.roles)
                if (!currentRoles.Contains(r))
                    _manager.AddToRole(id, r);
            foreach (var r in currentRoles)
                if (!account.roles.Contains(r))
                    _manager.RemoveFromRole(id, r);

            base.update(id, account, patch);
        }

        public virtual string create(Account account, string password)
        {
            if (_collection.AsQueryable().Any(a => account.UserName == a.UserName))
                throw new Exception("username exists");

            // create account
            _manager.Create(account, password);

            // return userid
            return _manager.FindByName(account.UserName).Id;
        }

        public override string create(Account account)
        {
            var pass = _generateRandomPassword(8);
            return create(account, pass);
        }

        public override void delete(string userid)
        {
            // read the account first
            var acc = _collection.AsQueryable().First(a => a.Id == userid);
            // TODO: check for null

            _manager.Delete(acc);
        }

        // generate random password 
        private readonly Random _rng = new Random();
        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private string _generateRandomPassword(int size)
        {
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = _chars[_rng.Next(_chars.Length)];
                if (_rng.Next(_chars.Length) > _chars.Length / 2)
                    buffer[i] = Char.ToLower(buffer[i]);
            }
            return new string(buffer);
        }

    }
}