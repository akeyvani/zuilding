﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
//using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Zuilding.Web.Domain;
using Zuilding.Web.Domain.Tenant;

namespace Zuilding.Web.Repository
{
    public class BuilderRepository: AccountsRepository
    {
        public override string create(Account c, string password)
        {
            // existing tenant?
            var initTenant = base.getByTenantId(c.tenantId).FirstOrDefault() == null;

            // create the account
            c.Roles.Add("Builder");
            c.Roles.Add("TenantAdmin");
            var userid = base.create(c, password);

            // init tenant db if needed
            if (initTenant)
            {
                // create template for costs
                var ct = Proposal.buildDefaultCosts();
                var repo = new TenantRepository<Cost>(c.tenantId);
                foreach (var cost in ct)
                    repo.create(cost);
            }

            return userid;
        }

        public override void delete(string id)
        {
            base.delete(id);
        }
    }
}