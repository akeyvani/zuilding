﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
//using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Zuilding.Web.Domain;

namespace Zuilding.Web.Repository
{
    public class InvestorRepository : AccountsRepository
    {
        public string _creatorUserId { get; set; }

        public override string create(Account c)
        {
            // TODO: use enum
            c.Roles.Add("Builder");
            c.roles.Add("TenantAdmin");

            // will assume new tenant if tenantId is null. TODO: check for duplicate
            c.tenantId = c.tenantId ?? c.UserName;

            // create the account
            var userid = base.create(c);

            // add a contact refering to the creator user account
            if (_creatorUserId != null)
            {
                // Add the creator to this accounts contacts
                var creatorAccount = base.get(_creatorUserId);
                var repo = new TenantRepository<Contact>(c.tenantId);
                repo.create(new Contact { name = creatorAccount.UserName, userId = creatorAccount.Id, email = creatorAccount.email, roles = creatorAccount.roles });
            }

            return userid;
        }

        public override void delete(string id)
        {
            base.delete(id);
        }
    }
}