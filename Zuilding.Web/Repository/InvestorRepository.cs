﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Zuilding.Web.Domain;
using Zuilding.Web.EntityFramework;

namespace Zuilding.Web.Repository
{
    public class InvestorRepository : UserRepository
    {
        public InvestorRepository(int userid): base(userid) { 
        }

        public IEnumerable<Interest> getInterests()
        {
            return null;
        }

        public void removeInterestById(int interestId)
        {
        }

        public Interest createInterest(int proposlId)
        {
            return null;
        }
    }
}
