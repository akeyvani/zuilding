﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Zuilding.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
   //     private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        //private static bool _isInitialized;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            // "JSON.NET" setup
            // server json interface serialization/deserialization for enums -> string
            JsonConvert.DefaultSettings = (() =>
            {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add(new StringEnumConverter { CamelCaseText = true });
                return settings;
            });

            // MongoDB setup
            // ??
            //store enums as strings in mongo - TODO: dicionaries of enums??
            var c = new ConventionPack();
            c.Add(new EnumRepresentationConvention(BsonType.String));
            c.Add(new IgnoreIfNullConvention(true));

            ConventionRegistry.Register("enums-nulls", c, a => true);


     //       LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);

            //
            // JSON.NET settings - for enum types

            // var formatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            //formatter.SerializerSettings = new JsonSerializerSettings
            //{
            //    Formatting = Formatting.Indented,
            //    TypeNameHandling = TypeNameHandling.Objects,
            //    ContractResolver = new CamelCasePropertyNamesContractResolver()
            //};
            //formatter.SerializerSettings.Converters.Add( new StringEnumConverter { CamelCaseText = true });

            
            // DEBUG: when using Fiddler
            WebRequest.DefaultWebProxy = new WebProxy("127.0.0.1", 8888);
        }

        //public class SimpleMembershipInitializer
        //{
        //    public SimpleMembershipInitializer()
        //    {
        //        if (!WebSecurity.Initialized)
        //        {
        //            WebSecurity.InitializeDatabaseConnection("DefaultConnection", "Accounts", "userId", "name", autoCreateTables: true);
        //        }
        //    }
        //}
    }
}