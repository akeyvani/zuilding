﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Zuilding.Web.Domain;

namespace Zuilding.Web.Infrastructure
{ 
    public class ZuildingDb : DbContext
    {
        public ZuildingDb(): base("CodeFirstConnection")
        {
        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ZuildingDbConfiguration());
            
            base.OnModelCreating(modelBuilder);
        }
    }
}