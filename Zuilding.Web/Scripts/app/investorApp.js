﻿/// <reference path="viewmodels/contacts.js" />
/// <reference path="viewmodels/accounts.js" />
/// <reference path="viewmodels/proposal/container.js" />

function InvestorTenantVM() {
    var self = this;

    // portfolios
    this.role = 'Investor';
}


// create app
window.app = {};

// Load the Visualization API and chart packages
app.googleChartsLoaded = ko.observable(false);
google.load('visualization', '1.0', { 'packages': ['corechart'] });
google.setOnLoadCallback(function () {
    app.googleChartsLoaded(true);
});

// setup service
app.service = new Service('/api', [
   'accounts',
   'contacts',
   'proposals',
//   'projects',
//   'checklists',
//   'files',
//   'defaultCosts',
   'portfolios'
]);

// setup menu
app.menu = new MenuViewModel()
app.menu.items([
    { name: 'Proposals' },
    //{ name: 'Projects' },
    //{ name: 'Checklists' },
    { name: 'Contacts' },
    //{ name: 'Files' }
]);

app.tenant = new InvestorTenantVM();
app.accounts = new AccountsVM();
app.proposals = new ProposalContainerVM();
app.contacts = new ContactsContainerVM("contacts");
app.contacts.load();

app.menu.goto("Proposals");

// activate ko
ko.applyBindings(window.app);


