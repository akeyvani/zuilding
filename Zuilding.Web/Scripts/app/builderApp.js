﻿/// <reference path="viewmodels/contacts.js" />
/// <reference path="viewmodels/accounts.js" />
/// <reference path="viewmodels/proposal/container.js" />
/// <reference path="viewmodels/proposal/report.js" />

ko.validation.configure({
    registerExtenders: true,
    decorateElement: true,
    messagesOnModified: true,
    insertMessages: false,
    parseInputAttributes: true,
    messageTemplate: null
});


function BuilderTenantVM() {
    var self = this;

    // portfolios
    this.defaultCosts = ko.observableArray().endpointConnect({ type: 'defaultCosts' });
    this.defaultCosts.refresh();
    this.role = 'Builder';
}

// create app
window.app = {};

// Load the Visualization API and chart packages
app.googleChartsLoaded = ko.observable(false);
google.load('visualization', '1.0', { 'packages': ['corechart'] });
google.setOnLoadCallback(function () {
    app.googleChartsLoaded(true);
});

// setup service
app.service = new Service('/api', [
   'accounts',
   'proposals',
   'proposalHeaders',
   'projects',
   'checklists',
   'files',
   'defaultCosts',
]);

// setup menu
app.menu = new MenuViewModel()
app.menu.items([
    { name: 'Proposals' },
]);

app.tenant = new BuilderTenantVM();
app.accounts = new AccountsVM();

app.proposals = new ProposalContainerVM();

//app.contacts = new ContactsContainerVM();
//app.contacts.dataItems.refresh();

app.menu.goto("Proposals");

// activate ko
ko.applyBindings(window.app);


