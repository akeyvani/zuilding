﻿/// <reference path="list.js" />
/// <reference path="~/scripts/app/builderApp.js" />

function AccountVM(data, parent) {
    EntityVM.apply(this, [data, parent]);

    var self = this;

    // override
    this.templateNames.edit('zg-template-account-edit');
    this.templateNames.view('zg-template-account-view');

    // override cuz id is Id in this case
    this.Id = this.data.connect('Id');

    this.name = this.data.connect('UserName');
    this.email = this.data.connect('email');
    this.roles = this.data.connect('roles');
    this.tenantId = this.data.connect('tenantId');
}

function AccountsVM() {
    EntityListVM.apply(this, [{type:'accounts'}]);

    var self = this;

    // override item creation
    this.itemFactory.create = function (data) {
        data.id = data.Id;
        return new AccountVM(data, self);
    }

    // issue a get from ~/accounts to get logged in user
    this.currentUser = new AccountVM({}, self);
    this.currentUser.data.refresh().then(function () {
        // load all accounts if user is admin
        if (ko.utils.arrayIndexOf(self.currentUser.roles() || [], "TenantAdmin") > -1)
            self.dataItems.refresh(self.currentUser.tenantId());
    });

}