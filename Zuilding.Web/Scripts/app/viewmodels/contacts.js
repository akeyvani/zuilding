﻿/// <reference path="list.js" />
/// <reference path="~/scripts/app/builderApp.js" />

function ContactVM(data, parent) {
    EntityVM.apply(this, [data, parent]);
    var self = this;

    // override
    this.templateNames.edit('zg-template-contact-edit');
    this.templateNames.view('zg-template-contact-view');

    this.name = this.data.connect('name');
    this.email = this.data.connect('email');
    this.roles = this.data.connect('roles');
    this.userId = this.data.connect('userId');
    this.group = ko.computed(function () {
        var r = self.roles();
        for (var a in r)
            switch (r[a]) {
                case "Investor": return "Investors";
                case "Contractor": return "Contractors";
            }
    });

    // override
    this.matches = function (query) {
        return true;//(query == "All" || self.group() == query);
    }
}

function ContactsContainerVM() {
    EntityListVM.apply(this, [{ type: 'contacts' }]); // extend list for contacts
    var self = this;

    // override
    this.itemFactory.create = function (data) {
        var vm = new ContactVM(data, self);
        if (vm.isNew())
            vm.roles(["Investor"]);
        return vm;
    }
}



