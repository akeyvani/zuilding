﻿/// <reference path="list.js" />
/// <reference path="~/scripts/app/builderApp.js" />

ko.bindingHandlers.dropUpload = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var hoverState = allBindings.get('dragHover');

        // handles hover events - sets hover state        
        function dragHandler(evt) {
            evt.preventDefault();
            hoverState(evt.type == 'dragover')
            return false;
        }
        element.addEventListener('dragover', dragHandler, false);
        element.addEventListener('dragleave', dragHandler, false);
        element.addEventListener('drop', function (evt) {
            dragHandler(evt);
            var value = valueAccessor();
            value(evt.dataTransfer.files);
        }, false);
    }
};
function FileVM(data, parent) {
    EntityVM.apply(this, [data, parent]);

    var self = this;

    // upload
    this.isUploading = ko.observable();
    this.progress = ko.observable(0);
    this.uploadSuccess = ko.observable();
    this.uploadFailure = ko.observable();
    this.uploadText = ko.computed(function () {
        var msg = 'Uploading...';
        if (self.uploadSuccess())
            msg = 'Upload Done.';
        else
            if (self.uploadFailure())
                msg = 'Upload FAILED.';
        return msg;
    });

    // override
    this.templateNames.edit('zg-template-file-edit');
    this.templateNames.view('zg-template-file-view');
    this.templateNames.current = ko.computed(function () {
        return !self.isNew() && self.editMode() ? self.templateNames.edit() : self.templateNames.view();
    })

    // data fields
    this.fileName = this.data.connect('fileName');
    this.type = this.data.connect('type');
    this.description = this.data.connect('description');
    this.fileId = this.data.connect('fileId');

    // link
    this.location = ko.computed(function () {
        return 'api/files/' + self.fileId() + '?getData=true';
    });

    // upload the given file object
    this.upload = function (f) {
        self.isUploading(true);

        var xhr = new XMLHttpRequest();
        if (xhr.upload) {
            // progress bar
            xhr.upload.addEventListener("progress", function (e) {
                var pc = parseInt(100 - (e.loaded / e.total * 100));
                self.progress(pc + "% 0");
            }, false);

            // file received/failed
            xhr.onreadystatechange = function (e) {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        // get the id of the newly saved file
                        var fileTag = JSON.parse(xhr.response);
                        self.id(fileTag.id);
                        self.fileId(fileTag.fileId);

                        console.log(self.data())
                        self.uploadSuccess(true);
                    }
                    else
                        self.uploadFailure(true);
                    self.isUploading(false);
                }
            };

            // start upload
            xhr.open("POST", $id("upload").action, true);

            var formData = new FormData();
            formData.append('file[]', f);

            xhr.send(formData);
        }
    }

    // upload the file if one is specified
    if (data.uploadFile && !data.uploadFileStarted) {
        this.upload(data.uploadFile);
        data.uploadFileStarted = true;
    }
}

function FilesVM(dataPath) {
    EntityListVM.apply(this, [{ type: 'files', path: dataPath || '' }]); // extend list for files
    var self = this;

    // override
    this.templateNames.headerContents('zg-template-file-list-upload-panel');
    this.itemFactory.create = function (data) {
        return new FileVM(data, self);
    }
    this.showUploadPanel = ko.observable(false);
    this.addNew = function () {
        self.showUploadPanel(true);
    }

    // state
    this.dragHover = ko.observable(false);
 
    // create vms for and upload given array of files
    this.upload = function (f) { 
        if (f.length > 0) {
            for (var i = 0; i < f.length; i++) {
                self.dataItems.push({uploadFile: f[i], fileName: f[i].name, type: f[i].type});
            }
            self.showUploadPanel(false);
        }
    }

    function filterFiles(type) {
        return ko.utils.arrayFilter(self.items(), function (a) {
            var t = a.type();
            return t && t.indexOf(type) >= 0;
        })
    }
    this.images = ko.computed(function() {
        return filterFiles('image');
    })

    this.pdfs = ko.computed(function() {
        return filterFiles('pdf');
    })
    this.find = function (id) {
        return ko.utils.arrayFirst(self.items(), function (a) {
            return a.id() == id;
        })
    }
    this.selectFiles = function (d, e) {
        self.upload(e.target.files);
    };
}



