﻿/// <reference path="page.js" />
/// <reference path="../../lib/entityList.js" />

function ContainerTabVM(data, parent) {
    var self = this;

    // data is either a proposal header or the strings "Create New" or "Index"
    this.data = ko.observable(data);
    this.name = ko.computed(function () {
        var d = self.data();
        return (typeof d == 'string') ? d: d.name();
    });
    this.rightAlign = ko.computed(function () {
        return self.name() == "Create New";
    });
    this.isActive = ko.computed(function () {
        return parent.activePage() == self.data();
    });
    this.cursor = ko.computed(function () {
        return self.isActive() ? 'auto' : 'pointer';
    });
    // disabled if this is the 'Create New' tab and a create new proposal is in progress
    this.disabled = ko.computed(function () {
        return self.name() == "Create New" && parent.addingNewMode();
    })
    this.activate = function () {
        var header = self.data();
        if (header == "Index")
            parent.showIndex();
        else
        {
            // Create a new header if we're adding a proposal
            if (header == "Create New")
                header = parent.addNew({ name: 'New Proposal' });

            // open the page
            header.open();
        }
    }
}

function ProposalHeaderVM(data, parent) {
    var self = this;

    EntityVM.apply(this, [data, parent]);

    // fields
    this.name = this.data.connect('name');
    this.dateCreated = this.data.connect('dateCreated');
    this.dateModified = this.data.connect('dateModified');

    // actual proposal page
    this.page = ko.observable(null);
    this.open = function () {
        var p = self.page();
        if (!p)
        {
            // create new vm ad load data
            p = new ProposalPageVM(self);
            if (!p.isNew())
                p.load();
            self.page(p);
        }
        // goto page
        parent.openTab(self);
    }
    this.close = function () {
        // cancel any unsaved changes, remove if new unsaved
        self.cancel();

        // close the tab
        parent.closeTab(self);

        // delete proposal
        self.page(null);
    }

    // override - modified when there's a page and it's modified
    this.modifed = ko.computed(function () {
        var p = self.page();
        return p && p.modified();
    })
}

// Contains an index of proposal headers, shown on index page. Each header
// can potentially point to an open proposal page. 
// For each open page there is a tab at the top.
function ProposalContainerVM() {
    var self = this;

    EntityListVM.apply(this, [{ type: 'proposalHeaders' }]);
    this.itemFactory.create = function (data) {
        return new ProposalHeaderVM(data, self);
    }

    // currently active page shown to user
    this.activePage = ko.observable();

    // tabs shown up top
    this.tabs = ko.observableArray();
    this.findTab = function(header) {
        return ko.utils.arrayFirst(self.tabs(), function(t) {
            return t.data() == header;
        })
    }
    this.openTab = function(header) {
        // find corresponding tab - create new if not found
        var tab = self.findTab(header);
        if (!tab) {
            tab = new ContainerTabVM(header, self);
            self.tabs.push(tab);
        }
        // change active page
        self.activePage(tab.data());
    }
    this.closeTab = function (header) {
        // remove tab
        self.tabs.remove(self.findTab(header));
        // change active page
        self.showIndex();
    }

    // goto index page
    this.showIndex = function() {
        self.activePage("Index");
    }

    // ui
    this.afterProposalPageAdd = function (element, index, proposalPage) {
        $(element).fadeIn();
    }

    // default tabs
    this.tabs.push(new ContainerTabVM("Index", this));
    this.tabs.push(new ContainerTabVM("Create New", this));

    // load index
    this.dataItems.refresh();
    this.showIndex();
}
