﻿/// <reference path="~/scripts/knockout-3.0.0.debug.js" />
/// <reference path="~/scripts/jquery-2.1.0.js" />
/// <reference path="~/scripts/jquery-2.1.0.intellisense.js" />
/// <reference path="~/scripts/app/builderApp.js" />

// creates and displays a google chard object inside the node being bound,
// data comes from chartData binding, options come from chartOptions
//
ko.bindingHandlers.chartData = {
    init: function (element, valueAccessor, allBindings) {
        // create a chart and save it in the chart data observable 
        var data = valueAccessor();
        var graphType = allBindings.get('chartType') || 'PieChart'; // make pie chart default 
        element.chart = element.chart || new google.visualization[graphType](element);

        // set observable on selection
        var selection = allBindings.get('selection');
        if (selection) {
            google.visualization.events.addListener(element.chart, 'select', function () {
                selection(element.chart.getSelection());
            });
            selection.subscribe(function (val) {
                element.chart.setSelection(val);
            })
        }
    },
    update: function (element, valueAccessor, allBindings) {
        var data = valueAccessor();
        var options = ko.unwrap(allBindings.get('chartOptions'));
        var chart = element.chart;
        
        chart.draw(ko.unwrap(data), options);
    }
}

function FinancialsVM(proposal) {
    var self = this;

    //
    // Cost Groups
    //this.draw = ko.computed(function () {
    //    return proposal.activeSection
    //});
    this.costGroups = ko.observableArray([]).extend( { rateLimit: 0 });
    this.costs = ko.computed(function () {

        // make a data matrix
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Group');
        data.addColumn('number', 'Amount');

        self.costGroups([]);
        for (var i in proposal.specs.costGroups) {
            // one row for each cost group
            var cg = proposal.specs.costGroups[i];
            data.addRow([cg.title(), cg.total()]);

            // now create the cost breakdown of that group
            var bdData = new google.visualization.DataTable();
            bdData.addColumn('string', 'Cost');
            bdData.addColumn('number', 'Amount');
            var costs = cg.actives();
            for (var j in costs) {
                bdData.addRow([costs[j].name(), Number(costs[j].actualAmount())]);
            }

            // set the title and push it on costGroups
            bdData.groupTitle = proposal.specs.costGroups[i].title();
            self.costGroups.push(bdData);
        }
        self.costGroups.valueHasMutated();

        return data;
    })
    this.costGroupSelection = ko.observable();
    this.currentCostGroup = ko.computed(function() {
        var s = self.costGroupSelection();
        // return selected row == index to costGroups
        if (s && s.length > 0 && s[0].row != null)
            return self.costGroups()[s[0].row];

        return null;
    })
    this.ror = ko.computed(function () {
        return proposal.specs.totalSales() - proposal.specs.costs.total();
    });

    //
    // ROR
    this.rorGraph = ko.computed(function () {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Group');
        data.addColumn('number', 'Amount');
        data.addRow(['Hard Costs', proposal.specs.costGroups.hard.gross()]);
        data.addRow(['Soft Costs', proposal.specs.costGroups.soft.gross()]);
        data.addRow(['Land Costs', proposal.specs.costGroups.land.total()]);
        var c = proposal.specs.costGroups.management.find("marketing");
        if (c)
            data.addRow(['Marketing', Number(c.actualAmount())]);
        data.addRow(['ROR', self.ror()]);
        return data;
    })

    //
    // Investment
    this.investment = ko.computed(function () {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Time (month)');
        data.addColumn('number', 'Cumilitve Investment');
        data.addColumn('number', 'Average Monthly Investment');
        var months = proposal.cashFlow.months();
        var av = proposal.cashFlow.averageInvestment();
        for (var i in months)
            data.addRow( [ i, months[i].investment.adjusted(), av ]);
        return data;
    })

    //
    // Interest Payments
    this.interest = ko.computed(function () {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Time (month)');
        data.addColumn('number', 'Interest Payment');
        var months = proposal.cashFlow.months();
        for (var i in months)
            data.addRow([i, Number(months[i].interestPayment())]);
        return data;
    })

    //
    // table of report contents
    this.toc = ko.computed(function () {
        var t = new tocVM();
        t.push(['ror', 'Return on Revenue']);
        t.push(['costs', 'Costs']);
        return t;
    });
}
