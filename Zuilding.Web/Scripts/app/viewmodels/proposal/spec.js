﻿/// <reference path="~/scripts/app/builderApp.js" />

ko.bindingHandlers.uniqueId = {
    init: function (element, valueAccessor) {
        var value = valueAccessor();
        value.id = value.id || ko.bindingHandlers.uniqueId.prefix + (++ko.bindingHandlers.uniqueId.counter);

        element.id = value.id;
    },
    counter: 0,
    prefix: "unique"
};

ko.bindingHandlers.uniqueFor = {
    init: function (element, valueAccessor) {
        var value = valueAccessor();
        value.id = value.id || ko.bindingHandlers.uniqueId.prefix + (++ko.bindingHandlers.uniqueId.counter);

        element.setAttribute("for", value.id);
    }
};

ko.bindingHandlers.vvalue = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var v = valueAccessor();
        v = v.formattedValue || v;

        // set value binding
        ko.applyBindingsToNode(element, { value: v }, bindingContext);

        // setr validation element that gets the css change on error
        ko.applyBindingsToNode(element, { validationElement: v }, bindingContext);

        // init tipsy. show any error.
        $(element).tipsy({ title: function () { return v.error || ''; }});
    }
}

//
// custom binding for select elements showing optional costs
//
ko.bindingHandlers.costOptions = {
    init: function (element, valueAccessor, allBindings, viewModel, context) {
        var costs = valueAccessor();

        // add to cost array: an observable for current selection, and add function that adds the selection, and
        costs.currentOption = ko.observable();
        costs.activateCurrentOption = function () {
            ko.utils.arrayFirst(costs(), function (c) { return c.name() == costs.currentOption() }).isOptionalActive(true);
        }

        ko.applyBindingsToNode(element, {
            value: costs.currentOption,
            options: ko.computed(function () { return costs.options().map(function (c) { return c.name; }) })
        })
    },
};

ko.subscribable.fn.type = function (type) {
    var self = this;

    switch (type) {
        case 'integer':
            return this.extend({ pattern: { params: /^\d+$/, message: 'Please enter a non-negative integer value.' } });

        case 'decimal':
        case 'currency':
        case 'area':
            this.formattedValue = ko.computed({
                read: function () {
                    return +Number(self()).toFixed(2);
                },
                write: function (a) {
                    self(a);
                }
            }).extend({ number: true, min: 0 });
            
            return this;

        case 'percentage':
            this.formattedValue = ko.computed({
                read: function () {
                    return +(self() * 100).toFixed(2);
                },
                write: function (a) {
                    self(a / 100);
                }
            }).extend({ number: true, min: 0, max: 100 });

            return this;
    }
}

ko.subscribable.fn.multiply = function (other) {
    var self = this;

    return ko.computed({
        read: function () {
            return self() * ko.unwrap(other);
        },
        write: function (val) {
            var otherVal = ko.unwrap(other);
            if (otherVal != 0)
                self(val / otherVal);
            else {
                // else ERROR
            }
        }
    })
}

ko.subscribable.fn.divide = function (other) {
    var self = this;

    return ko.computed({
        read: function () {
            return ko.unwrap(other) / self();
        },
        write: function (val) {
            other(self() * val);
        }
    })
}

//
// Buildable Area View Model
//
function BuildingVM(b, cost) {
    var self = this;

    this.name = cost.name;
    this.isOptional = cost.isOptional;
    this.id = b.id;

    this.data = ko.observable(b || {}).track(this)
    this.area = this.data.connect('area').type('area'); 
    this.percentSellable = this.data.connect('percentSellable').type('percentage'); 
    this.salePrice = this.data.connect('salePrice').type('currency'); 
    this.numUnits = this.data.connect('numUnits').type('integer');

    this.areaSellable = this.percentSellable.multiply(this.area).type('area');
    this.areaSales = this.salePrice.multiply(this.areaSellable).type('currency');

    this.deactivate = function () {
        cost.deactivate();
    }
}

function LandVM(l, cost) {
    var self = this;

    this.data = ko.observable(l || {}).track(this);

    this.address = this.data.connect('address');
    this.area = this.data.connect('area').type('area');
    this.FSR = this.data.connect('FSR').type('decimal');
    this.buildableArea = this.FSR.multiply(this.area).type('area');
    this.buildingArea = this.data.connect('buildingArea').type('area');
    this.deactivate = function () {
        cost.deactivate();
    }
}

function CostVM(c, spec) {
    var self = this;

    this.id = c.id;
    this.data = ko.observable(c);
    this.data.track(this.data);
    this.name = this.data.connect('name');
    this.groupName = this.data.connect('groupName');
    this.amount = this.data.connect('amount');
    this.isOptional = this.data.connect('isOptional');
    this.isOptionalActive = this.data.connect('isOptionalActive');
    this.isActive = ko.computed(function () {
        return !self.isOptional() || self.isOptionalActive();
    });
    this.buildableArea = c.groupName == "hard" ? new BuildingVM(c.buildableArea, this) : null;
    this.land = (c.groupName == "land" && c.name == "price") ? new LandVM(c.land, this) : null;

    // '-' button
    this.deactivate = function () {
        self.isOptionalActive(false);
    }

    this.modified = ko.computed(function () {
        return self.data.modified() || (self.buildableArea && self.buildableArea.modified()) || (self.land && self.land.modified());
    })

    // actualAmount
    switch (c.groupName) {
        case "land":
            this.actualAmount = this.amount = this.amount.type('area');
            break;
        case "soft":
            // percent * total hard costs
            this.amount = this.amount.type('percentage');
            this.actualAmount = this.amount.multiply(spec.costGroups.hard.total).type('currency');
            break;

        case "hard":
            // area * price
            this.amount = this.amount.type('currency');
            this.actualAmount = this.amount.multiply(this.buildableArea.areaSellable).type('currency');
            break;

        case "management":
            this.amount = this.amount.type('percentage');
            switch (c.name) {
                case "marketing":
                    // percent * total sales
                    this.actualAmount = this.amount.multiply(spec.totalSales).type('currency');
                    this.multiplierText = ko.observable('of total sales');
                    break;

                case "construction":
                    // percent * total hard costs
                    this.actualAmount = this.amount.multiply(spec.costGroups.hard.total).type('currency');
                    this.multiplierText = ko.observable('of total hard costs');
                    break;

                case "development":
                    // percent * (total hard + soft costs)
                    var hardSoftTotal = ko.computed(function () { return spec.costGroups.hard.total() + spec.costGroups.soft.total(); });
                    this.actualAmount = this.amount.multiply(hardSoftTotal).type('currency');
                    this.multiplierText = ko.observable('of total hard and soft costs');
                    break;
            }
            break;

        case "contingency":
            this.amount = this.amount.type('percentage');
            switch (c.name) {
                case "hard":
                    this.actualAmount = this.amount.multiply(spec.costGroups.hard.total).type('currency');
                    this.multiplierText = ko.observable('of total hard costs');
                    break;

                case "soft":
                    this.actualAmount = this.amount.multiply(spec.costGroups.soft.total).type('currency');
                    this.multiplierText = ko.observable('of total soft costs');
                    break;
            }
            break;

        default:
            this.amount = this.amount.type('currency');
            this.actualAmount = this.amount;
            break;
    }
}

//
// cost groups (by groupName)
function CostGroupsVM(costs) {
    var self = this;

    // build a projection consisting of: active items, optional items, total, etc.
    function buildCostGroup(groupName, costs) {
        var costGroup = ko.computed(function () {
            return costs().filter(function (c) { return c.groupName() == groupName; });
        });

        costGroup.title = ko.computed(function () {
            return groupName + ' costs';
        });
        costGroup.tag = ko.computed(function () {
            return groupName + 'Costs';
        });

        function filterCosts(c, getActives) {
            return ko.utils.arrayFilter(c, function (a) {
                var b = a.isActive();
                return (getActives && b) || (!getActives && !b);
            });
        }
        costGroup.options = ko.computed(function () {
            return filterCosts(costGroup(), false);
        });
        costGroup.actives = ko.computed(function () {
            return filterCosts(costGroup(), true);
        });

        costGroup.total = ko.computed(function () {
            var total = 0;
            ko.utils.arrayForEach(costGroup.actives(), function (c) {
                total += Number(c.actualAmount());
            });

            return total;
        });
        costGroup.find = function (name) {
            return ko.utils.arrayFirst(costGroup() || [], function (c) {
                return c.name() == name;
            });
        }
        return costGroup;
    }

    this.hard = buildCostGroup('hard', costs);
    this.soft = buildCostGroup('soft', costs);
    this.land = buildCostGroup('land', costs);
    this.management = buildCostGroup('management', costs);
    this.contingency = buildCostGroup('contingency', costs);

    this.contingency.hard = ko.computed(function () { return self.contingency.find('hard'); });
    this.contingency.soft = ko.computed(function () { return self.contingency.find('soft'); });
    this.hard.gross = ko.computed(function () {
        var c = self.contingency.hard();
        return c ? c.actualAmount() + self.hard.total() : 0;
    })
    this.soft.gross = ko.computed(function () {
        var c = self.contingency.soft();
        return c ? c.actualAmount() + self.soft.total() : 0;
    })
}

//
// Proposal Spec View Model - pass in proposal page object
function ProposalSpecVM(proposal) {
    var self = this;

    this.data = proposal.data;
    this.id = proposal.id;
    this.title = this.data.connect('name').extend({ required: true, minLength: 6, maxLength: 36 });
    this.requiredEquity = this.data.connect('requiredEquity').type('currency');
    this.projectDuration = this.data.connect('projectDuration').type('integer');
    this.interestRate = this.data.connect('interestRate').type('percentage');
    this.costsData = this.data.connect('costs');
    this.costs = ko.computed(function () {
        return (self.costsData() || [] ).map(function (c) { return new CostVM(c, self); });
    });
    this.costs.modified = ko.computed(function () {
        return !!ko.utils.arrayFirst(self.costs(), function (c) {
            return c.modified();
        })
    })
    this.modified = ko.computed(function () {
        return self.data.modified() || self.costs.modified();
    })
    this.costGroups = new CostGroupsVM(this.costs);
    this.costs.total = ko.computed(function () {
        var tot = 0;
        for (var i in self.costGroups) {
            tot += self.costGroups[i].total();
        }

        // subtract marketing costs from total costs
        var marketingCost = ko.utils.arrayFirst(self.costGroups.management.actives(), function (c) {
            return c.name() == 'marketing';
        })
        tot -= marketingCost ? marketingCost.actualAmount() : 0;
        
        return tot;
    });
    this.percentEquity = this.costs.total.divide(this.requiredEquity).type('percentage');

    // residential, commercial, parking, etc.
    this.buildings = ko.computed(function () {
        return self.costGroups.hard.actives().map(function (c) { return c.buildableArea; }) || [];
    })
    this.totalToBuild = ko.computed(function () {
        var total = 0;
        ko.utils.arrayForEach(self.buildings(), function (c) {
            total += Number(c.area());
        });
        return total || 0;
    });
    this.totalSales = ko.computed(function () {
        var total = 0;
        for (var i in self.buildings())
            total += Number(self.buildings()[i].areaSales());
        return total;
    })
    // land
    this.lands = ko.computed(function () {
        return self.costGroups.land().filter(function (c) {
            return (c.name() == "price");
        }).map(function (c) {
            return c.land
        }) || [];
    })
    this.lands.totals = ko.computed(function () {
        var t = {
            land: 0,
            building: 0,
            buildable: 0,
        };
        ko.utils.arrayForEach(self.lands(), function (l) {
            t.land += Number(l.area());
            t.building += Number(l.buildingArea());
            t.buildable += Number(l.buildableArea());
        })
        return t;
    })
    this.lands.addNew = function () {
        self.costsData().push({
            groupName: 'land',
            name: 'price',
            land: {}
        })
        self.costsData.notifySubscribers();
    }

    // init new proposal spec
    if (proposal.isNew()) {
        this.costsData(app.tenant.defaultCosts());
    }
    
    // table of spec contents
    this.toc = ko.computed(function () {
        var toc = new tocVM();
        toc.push(['title', 'Title']);
        toc.push(['land', 'Land']);
        toc.push(['building', 'Building Areas']);
        // costs
        var c = [];
        for (var i in self.costGroups)
            if (self.costGroups[i].tag() != "contingencyCosts")
                c.push([self.costGroups[i].tag(), self.costGroups[i].title()]);
        toc.push(['costs', 'Costs', c]);
        // financing
        toc.push(['financing', 'Financing']);

        return toc;
    });
}
