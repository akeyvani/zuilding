﻿/// <reference path="~/scripts/knockout-3.0.0.debug.js" />
/// <reference path="~/scripts/jquery-2.1.0.intellisense.js" />
/// <reference path="~/scripts/app/lib/entityList.js" />
ko.bindingHandlers.editableHTML = {
    init: function (element, valueAccessor) {
        var $element = $(element);
        var initialValue = ko.utils.unwrapObservable(valueAccessor());
        $element.html(initialValue);
        $element.on('keyup', function () {
            observable = valueAccessor();
            observable($element.html());
        });
    }
};
ko.bindingHandlers.jmedia = {
    update: function (element, valueAccessor, allBindings) {
        var file = ko.unwrap(valueAccessor());
        if (file) {
            var options = {
                //width: 500,
                //heigh: 400,
                src: file.location(),
                type: 'pdf'
            }
            var a = $('<a></a>');
            $(element).empty();
            a.appendTo(element);
            a.media(options)
            //$(element).append()
            //.media(options);

            //$(element).prop('href', file.location
        }
    }
}

function CustomTabVM(data, parent, files) {
    var self = this;

    EntityVM.apply(this, [data, parent]);
    this.name = this.data.connect('name');
    this.label = this.data.connect('label');
    this.type = this.data.connect('type');

    switch (this.type()) {
        case 'pdf':
            this.pdfs = files.pdfs;
            this.fileTag = this.data.connect('fileTag');
            this.selectedFile = ko.observable(this.fileTag());
            this.selectedFile.subscribe(function (a) {
                var oldVal = self.fileTag();
                if (!!a && (!oldVal || oldVal.id != a.id)) {
                    self.fileTag(a);
                }
            })
            this.file = ko.computed(function () {
                var tag = self.fileTag();
                return tag ? files.find(tag.id): null;
            });
            break;

        case 'html':
            this.htmlData = this.data.connect('htmlData');
            break;
    }
}

function CustomTabListVM(files) {
    EntityListVM.apply(this, [{}]); // extend list for custom tabs - no type - no server counterpart
    var self = this;
    this.itemFactory.create = function (data) {
        return new CustomTabVM(data, self, files);
    }

    this.init = function (defaultTabs) {
        // add any default tabs that we dont have
        ko.utils.arrayForEach(defaultTabs, function (c) {
            if (!ko.utils.arrayFirst(self.items(), function (t) {
                return (ko.unwrap(t.name) == c.name);
            })) {
                self.dataItems.push(c);
            }
        })
        self.dataItems.notifySubscribers();
    }
    this.addTab = function (type) {
        var randomName = Math.random().toString(36).substring(7);
        self.addNew({ type: type, name: randomName, label: 'New Tab', id: 'xxx' });
        return randomName;
    }
}
