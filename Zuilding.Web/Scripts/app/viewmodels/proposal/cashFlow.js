﻿/// <reference path="~/scripts/knockout-3.0.0.debug.js" />
/// <reference path="~/scripts/jquery-2.1.0.js" />
/// <reference path="~/scripts/jquery-2.1.0.intellisense.js" />
/// <reference path="~/scripts/app/viewmodels/list.js" />
ko.subscribable.fn.paysAccount = function (prevMonthSnapshot, adjustment) {
    var self = this;

    var acc = ko.computed(function () {
        return Number(self()) + ko.unwrap(prevMonthSnapshot);
    });
    acc.adjustment = ko.computed(function () {
        return Math.min(acc(), Number(ko.unwrap(adjustment)));
    });
    acc.adjusted = ko.computed(function () {
        return acc() - acc.adjustment();
    })

    return acc;
}


function MonthVM(data, prevMonth, proposal) {
    var self = this;
    var prevLoan = prevMonth ? prevMonth.loan.adjusted : 0;
    var prevInv = prevMonth ? prevMonth.investment.adjusted : 0;
    
    this.data = ko.observable(data).track(this);

    this.sales = this.data.connect('sales').type('currency');
    this.loanInstallment = this.data.connect('loanInstallment').type('currency');
    this.investmentInstallment = this.data.connect('investmentInstallment').type('currency')

    this.loan = this.loanInstallment.paysAccount(prevLoan, this.sales);
    this.interestPayment = ko.computed(function () {
        return +(proposal.specs.interestRate() * self.loan.adjusted() / 12).toFixed(2);
    });
    this.investment = this.investmentInstallment.paysAccount(prevInv, ko.computed(function () {
        return self.sales() - self.loan.adjustment();
    }))
    
    // init empty object
    if (!data.hasOwnProperty('sales')) {
        this.sales(0);
        this.loanInstallment(0);
        this.investmentInstallment(0);
    }
}
function CashFlowVM(proposal) {
    var self = this;
    
    this.data = ko.observableArray([]).extend({ rateLimit: 0 });
    this.months = ko.observableArray().extend({ rateLimit: 0 });
    this.interestRate = proposal.specs.interestRate;
    this.data.subscribe(function (d) {
        if (d) {
            // instantiate a MonthVM for object in data
            var prevMonth = null;
            self.months([]);
            for (var i = 0; i < d.length; i++) {
                var m = new MonthVM(d[i], prevMonth, proposal);
                self.months.push(m)
                prevMonth = m;
            }
        }
    })
    this.modified = ko.computed(function () {
        return !!ko.utils.arrayFirst(self.months(), function (m) {
            return m.modified();
        })
    })
    this.clearModified = function () {
        ko.utils.arrayForEach(self.months(), function (a) {
            a.modified(false);
        })
    }

    this.intervalLength = proposal.specs.projectDuration;
    this.intervalLength.subscribe(function (val) {
        var d = self.data();
        var l = d.length;
        if (val > l) {
            // create empty months
            for (var i = 0; i < val - l; i++)
                d.push({});
            self.data.valueHasMutated();
        } else if (val < l) {
            // remove months
            self.data.splice(val - l, l - val);
        }
    })
    this.averageInvestment = ko.computed(function () {
        var tot = 0;
        ko.utils.arrayForEach(self.months(), function (m) {
            tot += m.investment.adjusted();
        })
        return tot / self.months().length;
    })
    this.requiredFinancing = ko.computed(function () {
        return proposal.specs.costs.total() - proposal.specs.requiredEquity();
    });

    // TODO: remove
    this.toc = ko.observable(new tocVM());
}
