﻿/// <reference path="graphs.js" />
/// <reference path="spec.js" />
/// <reference path="cashflow.js" />
/// <reference path="../files.js" />

//
// custom binding adds bootstrap scrollspy, binding specifies options and contents ready flag
//
ko.bindingHandlers.bsScrollSpy = {
    update: function (element, valueAccessor, allBindings, viewModel, context) {
        var hasContent = ko.unwrap(valueAccessor());
        if (hasContent) {
            $('body').scrollspy({ target: '.zg-sidebar', offset: 320 });
        }
    }
};

function ProposalPageVM(header) {
    var self = this;

    EntityVM.apply(this, [{}, self, { type: 'proposals' }])

    // fields 
    this.name = this.data.connect('name');

    // sync id and name
    this.id(header.id());
    this.id.subscribe(function (id) {
        header.id(id);
        header.data.modified(false);
    })
    this.name(header.name());
  
    // specs
    this.specs = new ProposalSpecVM(this);

    // cash flow
    this.cashFlow = new CashFlowVM(this);

    // Financials
    this.financials = new FinancialsVM(this);

    // files 
    this.files = new FilesVM();
    
    // active section
    this.activeSection = ko.observable('specs');

    // default sections
    this.sections = [
        { id: 1, label: 'Specifications', name: 'specs' },
        { id: 2, label: 'Cash Flow', name: 'cashflow' },
        { id: 3, label: 'Financials', name: 'financials' },
        { id: 5, label: 'Files', name: 'files' }
    ];
    this.trash = ko.observableArray();

    // section tabs at the top of proposal page
    this.tabs = new CustomTabListVM(this.files);
    this.addTab = function (type) {
        var name = self.tabs.addTab(type);
        self.activeSection(name);
    }
    this.addHTML = function () {
        return self.addTab('html');
    }
    this.addPDF = function () {
        return self.addTab('pdf');
    }
    // init tabs if new proposal
    if (this.isNew()) {
        this.tabs.init(this.sections);
    }

    // modified if constituents parts are modified
    this.modified = ko.computed(function () {
        return self.specs.modified() || self.cashFlow.modified() || self.files.modified() || self.tabs.modified();
    })

    this.close = function () {
        header.close();
    }

    // table of contents
    this.sidebar = ko.computed(function () {
        var toc = null;
        switch (self.activeSection()) {
            case 'specs':
                toc = self.specs.toc();
                break;
            case 'cashflow':
                toc = self.cashFlow.toc();
                break;
        }
        return toc;
    });
    
    // ajax
    this.load = function () {
        self.data.refresh().then(function() {

            // should: be able to remove these
            // read data into various vms
            var p = self.data();
            self.cashFlow.data(p.cashFlow);
            self.tabs.dataItems(p.tabs);
            self.files.dataItems(p.files);

            // state
            self.files.clearModified(false);
            self.tabs.clearModified(false);
            self.cashFlow.clearModified();
        })
    }

    // override save
    this._save = this.save;
    this.save = function () {

        var p = self.data();
        // need: to be able to remove these
        p.cashFlow = self.cashFlow.data();
        p.tabs = self.tabs.dataItems();
        p.files = self.files.dataItems();

        return self._save().done(function () {
            self.files.clearModified(false);
            self.tabs.clearModified(true);
            self.cashFlow.clearModified(false);
            
            // update header
            header.data.refresh();
        })
    }

}

