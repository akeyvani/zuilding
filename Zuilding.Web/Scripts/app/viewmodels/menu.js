﻿/// <reference path="~/scripts/knockout-3.0.0.debug.js" />
function MenuViewModel() {
    var self = this;

    // observables
    this.username = ko.observable();
    this.isAuthenticated = ko.observable(true);
    this.items = ko.observableArray([]);
    this.current = ko.observable('');

    // navigate
    this.goto = function (name) {
        // find menu item by name
        var t = self.items();
        var item = ko.utils.arrayFirst(self.items(), function (a) {
            return a.name == name;
        })
        // set to found item or passed in string
        self.current(item || name);
    }
};