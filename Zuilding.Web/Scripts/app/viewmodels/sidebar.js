﻿//
// custom binding adds bootstrap affix to any element, options supplied as binding value
//
ko.bindingHandlers.bsAffix = {
    update: function (element, valueAccessor, allBindings, viewModel, context) {
        var options = valueAccessor();
        $(element).affix(ko.unwrap(options));
    }
};

function tocEntryVM(data) {
    this.tag = ko.computed(function () { return data[0]; })
    this.title = ko.observable(data[1]);

    // submenu
    this.submenu = ko.observableArray();
    if (data[2]) {
        var sub = data[2];
        this.submenu(sub.map(function (d) {
            return new tocEntryVM(d);
        }));
    }
}

function tocVM() {
    // menu
    this.entries = ko.observableArray();
    this.push = function (entry) {
        this.entries.push(new tocEntryVM(entry));
    }
}
