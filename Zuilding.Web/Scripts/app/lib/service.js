﻿
//
// connects an observable to a service endpoint
ko.observable.fn.endpointConnect = function (resourceId) {
    var self = this;

    // state
    this.isLoading = ko.observable(false);
    this.isSaving = ko.observable(false);
    this.modified = ko.observable(false);

    // refresh causes entities to be loaded from server
    this.refresh = function (id) {
        self.isLoading(true);
        return app.service[resourceId.type].get(id || self().id, resourceId.path).then(function (d) {
            var data = self();
            if (!data)
                data = self({}); // create empty object the first time
            else
                for (var h in data) delete data[h]; // remove old properties

            // copy over received properties, and tell everyone
            for (var k in d) data[k] = d[k];
            self.notifySubscribers();
            self.isLoading(false);
            self.modified(false);
        });
    },
    this.save = function () {
        var data = self();
        var isNew = !data.id;
        var endpoint = app.service[resourceId.type];

        self.isSaving(true);
        var prom = isNew ? endpoint.post(data, resourceId.path) : endpoint.put(data, resourceId.path);
        return prom.always(function () {
            self.isSaving(false);
            self.modified(false);
        }).done(function (r) {
            if (isNew) {
                data.id = r.id || r; // return value is object itself or just the id
                self.notifySubscribers();
                self.modified(false);
            }
        })
    },
    this.delete = function () {
        self.isSaving(true);
        return app.service[resourceId.type].delete(self().id, resourceId.path).always(function () {
            self.isSaving(false);
        });
    }
    return this;
}

function Service(base, endpoints) {
    function ajaxData(type, url, data) {
        var options = {
            url: url,
            headers: {
                Accept: "application/json"
            },
            contentType: "application/json",
            //cache: false,
            type: type,
            data: data ? ko.toJSON(data) : null
        }
        return $.ajax(options);
    }

    function Endpoint(name, base) {
        var self = this;

        function _url(id, path) {
            id = (id || id === 0) ? id : '';
            return base + '/' + (path || '') + '/' + name + '/' + id;
        }

        this.get = function (id, path) {
            return ajaxData('get', _url(id, path));
        }
        this.index = function (id, path) { return self.get(id, path); }
        this.put = function (obj, path) {
            return ajaxData('put', _url(obj.id, path), obj);
        }
        this.post = function (obj, path) {
            return ajaxData('post', _url(null, path), obj);
        }
        this.delete = function (id, path) {
            return ajaxData('delete', _url(id, path));
        }
    }

    // add requested endpoints to this object
    for (var i in endpoints) {
        var e = endpoints[i];
        this[e] = new Endpoint(e, base);
    }
}


