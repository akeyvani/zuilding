﻿/// <reference path="~/scripts/knockout-3.0.0.debug.js" />
/// <reference path="~/scripts/jquery-2.1.0.js" />
/// <reference path="~/scripts/jquery-2.1.0.intellisense.js" />

// updates modFlag when observable changes
ko.subscribable.fn.track = function (context) {
    var self = this;

    // create the modFlag if one does not exist    
    context.modified = context.modified || ko.observable(false);

    this.subscribe(function (a) {
        context.modified(true);
    });

    return this;
}

// connect an observable to a property of another observable. adds modFlag.
ko.observable.fn.connect = function (property, validate) {
    /// <param name="data" type="ko.observable" />
    var self = this;

    return ko.computed({
        read: function () {
            // return object property
            return self()[property];
        },
        write: function (val) {
            var o = self();

            // set object property
            o[property] = val;

            // tell everyone 
            self.notifySubscribers(o);
        }
    }).track(this);
}

//
// Entity View Model
//
function EntityVM(data, parent, resourceId) {
    var self = this;

    // data
    this.data = ko.observable(data).endpointConnect(resourceId || parent.options.resourceId);

    // id
    this.id = this.data.connect('id');

    // page state
    this.modified = this.data.modified;
    this.isSaving = this.data.isSaving;
    this.isLoading = this.data.isLoading;
    this.formReady = ko.computed(function () {
        return !self.isSaving() && !self.isLoading();
    });
    this.isNew = ko.computed(function () {
        return !self.id();
    });
    this.editMode = ko.observable(false);

    // template names
    this.templateNames = {
        edit: ko.observable('el-template-edit'),
        view: ko.observable('el-template-view'),
    }
    this.templateNames.current = ko.computed(function () {
        return self.editMode() ? self.templateNames.edit() : self.templateNames.view();
    });

    // override to implement custom querying
    this.matches = function (query) {
        return true;
    }
    this.save = function () {
        return self.data.save().always(function () {
            self.editMode(false);
        })
    }
    this.edit = function () {
        self.editMode(true);
    }
    this.cancel = function () {
        // cancel adding new entity
        if (self.isNew()) {
            parent.remove(self);
            return;
        }
        // cancel any unsaved changes
        if (self.modified()) {
            self.data.refresh(self.id());
        }

        self.editMode(false);
    }
    this.del = function () {
        var prom = self.isNew() ? new Promise(function (resolve, fail) { resolve() }) : self.data.delete();
        return prom.then(function () {
            parent.remove(self);
        })
    }
}

//
// collections of entities
//
function EntityListVM(resourceId) {
    var self = this;

    //
    // options
    this.options = {
        allowEdit: true,
        allowAdd: true,
        allowRemove: true,
        resourceId: resourceId,
    }
    this.templateNames = {
        header: ko.observable('el-template-header'),
        list: ko.observable('el-template-list'),
        headerContents: ko.observable('el-template-header-contents')
    }

    //
    // entities
    this.vmItems = ko.observableArray([]).extend({ rateLimit: 0 });
    this.itemModified = ko.computed(function () {
        return !!ko.utils.arrayFirst(self.vmItems(), function (d) {
            return d.modified();
        })
    })
    this.dataItems = ko.observableArray([]).endpointConnect(resourceId);
    this.dataItems.subscribe(function () {
        // 
        self.vmItems(ko.utils.arrayMap(self.dataItems(), function (d) {
            // find the EntityVM whos data is 'd'
            var itemVM = ko.utils.arrayFirst(self.vmItems(), function (vm) {
                return vm.data() == d;
            });
            // create a new vm if one not found
            if (!itemVM) {
                itemVM = self.itemFactory.create(d);
                itemVM.modified(false);
                self.vmItems.push(itemVM);
            }
            return itemVM;
        }));
        self.vmItems.notifySubscribers();
    })
    this.dataItems.track(this.dataItems);

    // sort by id - override to customize
    this.compare = function (a, b) {
        var idA = a.id();
        var idB = b.id();
        return idA < idB ? -1 : (idA == idB ? 0 : +1);
    }
    this.items = ko.computed(function () {
        return self.vmItems().concat().sort(self.compare);
    })

    // 
    // query
    this.query = ko.observable(null);
    this.queryItems = ko.computed(function () {
        return ko.utils.arrayFilter(self.items(), function (a) {
            return a.matches(self.query());
        })
    });

    //
    // state
    this.modified = ko.computed(function () {
        return self.itemModified() || self.dataItems.modified();
    })
    this.isLoading = this.dataItems.isLoading;
    this.editMode = ko.computed(function () {
        return ko.utils.arrayFirst(self.items(), function (a) {
            a.editMode();
        })
    });
    this.addingNewMode = ko.computed(function () {
        return !!(ko.utils.arrayFirst(self.items(), function (a) {
            return a.isNew();
        }));
    })

    //
    // commands
    this.select = function (itemId) {
        self.currentItem = ko.utils.arrayFirst(this.items(), function (item) {
            return itemId == item.id();
        });
    }
    this.addNew = function (args) {
        // instantiate the item, data is null
        self.dataItems.push(args);
        self.dataItems.notifySubscribers();
        var item = ko.utils.arrayFirst(self.vmItems(), function (t) {
            return t.data() == args;
        });

        item.modified(true);
        item.editMode(true);

        return item;
    }
    this.remove = function (item) {
        self.dataItems.remove(item.data());
        self.dataItems.notifySubscribers();
    }
    this.clearModified = function (clearItems) {
        self.dataItems.modified(false);
        if (clearItems)
            ko.utils.arrayForEach(self.vmItems(), function (a) {
                a.modified(false);
            })
    }

    // builds item view models - override to create EntityVM derived objects
    this.itemFactory = {
        create: function (data) {
            return new EntityVM(data, self);
        }
    }
}

//
// register component
ko.components.register('entity-list', {
    template: {
        element: 'el-template-page'
    }
});
