jQuery(function() {

/* Fix Sizing */
	function fix_sizing() {
	
		if(jQuery('body').height() <= jQuery(window).height()) {
			jQuery('#site-footer').height(jQuery(window).height() - jQuery('#site-footer').offset().top);
		} else {
			jQuery('#site-footer').css({"height":"auto"});
		}
		
		if(jQuery('ul.back-slideshow li').size() > 0) {		
			jQuery('ul.back-slideshow li img').css({"width":"100%", "height":"auto"});		
			if(jQuery('ul.back-slideshow li:visible img').height() >= jQuery(window).height()) {
				jQuery('ul.back-slideshow li img').css({"width":"100%", "height":"auto"});		
			} else {	
				jQuery('ul.back-slideshow li img').css({"width":"auto", "height":"100%"});
			}
		}
		
	}
	jQuery(window).load(function() { fix_sizing(); });
	jQuery(window).resize(function() { fix_sizing(); });


/* Background Slideshow */
	if(jQuery('ul.back-slideshow').size() > 0) {
		jQuery('ul.back-slideshow li:first').fadeIn();
	}
	
	if(jQuery('ul.back-slideshow li').size() > 1) {
	
		var back_slide_to_show = 0;
		function change_back_slideshow_slide() {
			jQuery('ul.back-slideshow li').not(back_slide_to_show).stop(true,true).fadeOut(1000);
			jQuery('ul.back-slideshow li').eq(back_slide_to_show).stop(true,true).fadeIn(1000);
			fix_sizing();
		}
		setInterval(function() {
			back_slide_to_show++;
			if(back_slide_to_show >= jQuery('ul.back-slideshow li').size()) {
				back_slide_to_show = 0;
			}
			change_back_slideshow_slide();
		}, 4000);
	
	}


/* Work Thumbs Slideshow */
	if(jQuery('ul.back-portfolio-thumbs li').size() > 0) {
	
	jQuery('ul.back-portfolio-thumbs li img').each(function() {
		if(this.complete) {
			jQuery(this).animate({"opacity":"0.1"});
		} else {
			jQuery(this).load(function() {
				jQuery(this).animate({"opacity":"0.1"});
			});
		}
	});
	var random_item;
	var random_item_b;
	var random_item_html;
	var random_item_b_html;
	function change_work_thumbs() {
		random_item = Math.floor(Math.random()*jQuery('ul.back-portfolio-thumbs li').size());
		random_item_b = Math.floor(Math.random()*jQuery('ul.back-portfolio-thumbs li').size());
		random_item_html = jQuery('ul.back-portfolio-thumbs li').eq(random_item).html();
		random_item_b_html = jQuery('ul.back-portfolio-thumbs li').eq(random_item_b).html();
		jQuery('ul.back-portfolio-thumbs li').eq(random_item).find('img').animate({"opacity":"0"}, 800, function() {
			jQuery('ul.back-portfolio-thumbs li').eq(random_item).html(random_item_b_html);
			jQuery('ul.back-portfolio-thumbs li').eq(random_item).find('img').css({"opacity":"0"});
			jQuery('ul.back-portfolio-thumbs li').eq(random_item).find('img').animate({"opacity":"0.1"}, 800, function() {
				
				jQuery('ul.back-portfolio-thumbs li').eq(random_item_b).find('img').animate({"opacity":"0"}, 800, function() {
					jQuery('ul.back-portfolio-thumbs li').eq(random_item_b).html(random_item_html);
					jQuery('ul.back-portfolio-thumbs li').eq(random_item_b).find('img').css({"opacity":"0"});
					jQuery('ul.back-portfolio-thumbs li').eq(random_item_b).find('img').animate({"opacity":"0.1"}, 800, function() {
						
						setTimeout(function() { change_work_thumbs() }, 100);
						
					});
				});
				
			});
		
		});
	}
	change_work_thumbs();
	
	}


/* Progress */
	if(jQuery('.countdown').length > 0) {
	
		var progress = jQuery('.progress').attr('id');
		progress = progress.replace('progress-', '');
		jQuery('.progress span.percentage').css({"left":progress+"%", "opacity":"0"});
		jQuery('.progress span.completed').animate({"width":progress+"%"}, 600);
		jQuery('.progress span.percentage span').text(progress);
		
		if(jQuery('.progress span.percentage').position().left < 1) {
			jQuery('.progress span.percentage').animate({"left":"40px"});
		} else if(jQuery('.progress span.percentage').position().left >= jQuery('.progress').width()) {
			jQuery('.progress span.percentage').animate({"left":jQuery(window).width() - 60 + "px"});
		}
		jQuery('.progress span.percentage').animate({"opacity":"1"}, 1000);
	
	}


/* Countdown */
	if(jQuery('.countdown').length > 0) {
	
		var countdown = jQuery('.countdown').attr('id');
		countdown = countdown.replace('countdown-', '');
		countdown = countdown.split('-');
		
		function calc_countdown() {
			var countdown_date = new Date(countdown[0],countdown[1] - 1,countdown[2],countdown[3],countdown[4],'00');
			var current_date = new Date();
			var time_difference = countdown_date.getTime() - current_date.getTime();
			
			var seconds = Math.floor(time_difference/1000);
			var minutes = Math.floor(seconds/60);
			var hours = Math.floor(minutes/60);
			var days = Math.floor(hours/24);
			hours %= 24;
			minutes %= 60;
			seconds %= 60;
			if(seconds < 10) { seconds = 0 + '' + seconds; }
			if(minutes < 10) { minutes = 0 + '' + minutes; }
			if(hours < 10) { hours = 0 + '' + hours; }
			if(days < 10) { days = 0 + '' + days; }
	
			if(time_difference <= 0) {
				jQuery('.countdown h3').remove();
				jQuery('.countdown ul').remove();
				jQuery('.countdown p.launch-text').show();
			} else {
				jQuery('.countdown li.days span.number').text(days);
				jQuery('.countdown li.hours span.number').text(hours);
				jQuery('.countdown li.minutes span.number').text(minutes);
				jQuery('.countdown li.seconds span.number').text(seconds);
				var countdown_timer = setTimeout(function() { calc_countdown() }, 1000);
			}
			
		}
		calc_countdown();
		
	}
	
	
/* Email subscription form */
	//if(jQuery('.php-newsletter').length > 0) {
		
	//	jQuery('.php-newsletter #subscribe_submit').after('<input type="button" id="subscribe_submit" value="'+jQuery('.php-newsletter input#subscribe_submit').val()+'" />');
	//	jQuery('.php-newsletter input[type="submit"]').remove();
		
	//	jQuery('.php-newsletter input#subscribe_submit').click(function() {
				
	//		jQuery.ajax({
	//			type: "POST",
	//			url: jQuery('.php-newsletter').attr('action'),
	//			data: { subscribe_email: jQuery('.php-newsletter input#subscribe_email').val(), subscribe_submit: 1 }
	//		}).done(function( msg ) {
	//			jQuery('.php-newsletter .response').html(msg);
	//		});
	//		return false;
		
	//	});
		
	//}

});