﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zuilding.Web.Domain.GooglePlaces
{
    // the lat and lng is teh proposal lat/lng
    // placeViewModel.lat/lng is the place view model
    public class MapPlacesViewModel
    {
        public List<PlaceViewModel> places { get; set; }
        public double lat { get; set; } 
        public double lng { get; set; }
    }
}