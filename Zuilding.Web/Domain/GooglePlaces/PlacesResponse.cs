﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zuilding.Web.Domain.GooglePlaces
{

    public class PlacesResponse
    {
        public string[] html_attributions { get; set; }
        public List<PlacesResults> results { get; set; }
        public string status { get; set; }
    }

    public class PlacesResults
    {
        public string vicinity { get; set; }
        public GeometryLoacation geometry { get; set; }
        public string name { get; set; }
        public string[] types { get; set; }
    }

    public class GeometryLoacation
    {
        public LocationInfo location { get; set; }
    }

    public class LocationInfo
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

}