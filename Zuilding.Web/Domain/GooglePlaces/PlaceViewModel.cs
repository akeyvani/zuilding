﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zuilding.Web.Domain.GooglePlaces
{
    public class PlaceViewModel
    {
        public string type { get; set; }
        public string name { get; set; }
        public string vicinity { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public double distance { get; set; }

    }
}