﻿using MongoDB.AspNet.Identity;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Zuilding.Web.Repository;

namespace Zuilding.Web.Domain
{
    //
    // This is the user pofile used by WebSecurity. 
    //
    public class Account : IdentityUser, ITenantEntity
    {
        [BsonIgnore]
        string ITenantEntity.id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        private ICollection<string> _roles;

        [BsonIgnore]
        public ICollection<string> roles { get { return Roles; } set { _roles = value; } }

        [JsonIgnore]
        public override List<string> Roles
        {
            get
            {
                return base.Roles;
            }
        
        }
        
        public string email { get; set; }
        public string tenantId { get; set; }

    }
}