﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Zuilding.Web.Repository;

namespace Zuilding.Web.Domain.Checklist
{
    
    public class Task: ITenantEntity
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string id { get; set; }


        public Task() { }
        public enum Role { Creator, Assigner, Assignee, Completer, Coordinator, Controller }
        public enum Action { Create, Add, Modify, Start, Complete }
        
        //[BsonId]
        //public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    
        //public List<KeyValuePair<Role, Contact>> participants { get; set; }
        //public List<KeyValuePair<Action, DateTime>> history { get; set; }
        //[JsonConverter(typeof(KeyValuePairConverter))]
        //public Dictionary<string, string> history { get; set; }
        // public Dictionary<Role, Contact> participants { get; set; }
        public Dictionary<Action, DateTime> history { get; set; }
        public List<Task> subTasks { get; set; }
        public List<FileTag> files { get; set; }
    }
}