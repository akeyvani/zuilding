﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zuilding.Web.Repository;

namespace Zuilding.Web.Domain
{
    public class Contact: ITenantEntity
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string id { get; set; }

        public string name { get; set; } // key
        public ICollection<string> roles { get; set; }
        public string email { get; set; }
        public string userId { get; set; }
        public bool hasAccount() { return userId != null; }

        [BsonIgnore]
        public bool requiresAccount { get; set; }
    }
}
