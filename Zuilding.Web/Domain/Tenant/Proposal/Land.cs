﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zuilding.Web.Domain
{
    public class Land
    {
        public string address { get; set; }
        public decimal area { get; set; }
        public decimal buildingArea { get; set; }
        public double FSR { get; set; }
        public decimal price { get; set; }
    }
}
