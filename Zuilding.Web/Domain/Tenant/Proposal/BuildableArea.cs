﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zuilding.Web.Domain
{
    public class BuildableArea
    {
        public int id {get; set;}
        public BuildableArea Clone() { return (BuildableArea)this.MemberwiseClone();  }
        public decimal? area { get; set; }
        public decimal? percentSellable { get; set; }
        public decimal? salePrice { get; set; }
        public int? numUnits { get; set; }
    }
}