﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zuilding.Web.Domain.Tenant
{
    public class CashFlowMonth
    {
        public decimal investmentInstallment { get; set; }
        public decimal loanInstallment { get; set; }
        public decimal sales { get; set; }
    }
}
