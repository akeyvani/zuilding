﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zuilding.Web.Domain.Tenant
{
    public class ProposalTab
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string id { get; set; }

        public enum Type { PDF, HTML };
        public string name { get; set; }
        public string label { get; set; }
        public string type { get; set; } // 'pdf' or 'html'
        public FileTag fileTag { get; set; } // for pdf
        public string htmlData { get; set; } // for html
    }
}
