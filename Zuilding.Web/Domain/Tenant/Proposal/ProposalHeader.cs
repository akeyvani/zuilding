﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Zuilding.Web.Repository;

namespace Zuilding.Web.Domain
{
    public class ProposalHeader: ITenantEntity
    {
        public string id { get; set; }
        public string name { get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime dateModified { get; set; }
        //public string portfolioName { get; set; }
    }
}