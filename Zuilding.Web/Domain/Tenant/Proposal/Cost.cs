﻿using AutoMapper;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Zuilding.Web.Domain.Tenant;
using Zuilding.Web.Repository;

namespace Zuilding.Web.Domain
{
    public class Cost: ITenantEntity
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string id { get; set; }

        public virtual Cost Clone()
        {
            var c = new Cost();
            return c.Copy(this);
        }

        protected virtual Cost Copy(Cost c)
        {
            Mapper.CreateMap<Cost, Cost>().ForSourceMember(a => a.buildableArea, opt => opt.Ignore()); ;
            Mapper.Map<Cost, Cost>(c, this);

            if (c.buildableArea != null)
                buildableArea = c.buildableArea.Clone();

            return this;
        }

        //[BsonIgnore]
        //[JsonIgnore]
        //public int id { get; set; }
        public string name { get; set; }
        public decimal? amount { get; set; }
        public string groupName { get; set; }
        public bool isOptional { get; set; }
        public bool? isOptionalActive { get; set; }
        public bool isHard() { return groupName == "hard"; }
        public BuildableArea buildableArea { get; set; }
        public Land land { get; set; }
    }
}
