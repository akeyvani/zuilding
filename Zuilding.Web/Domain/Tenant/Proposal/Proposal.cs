﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zuilding.Web.Repository;
using Zuilding.Web.Domain;
using MongoDB.Bson.Serialization.IdGenerators;
//using Zuilding.Web.Domain.Tenant.Proposal;
//using Zuilding.Web.Domain.Tenant.Proposal;
//using Zuilding.Web.Domain.Tenant;

namespace Zuilding.Web.Domain.Tenant
{
    public class Proposal : ITenantEntity
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string id { get; set; }
        // type
        public string buildingType { get; set; }

        public string name { get; set; }

        public IList<Land> lands { get; set; }
        // financing
        public double requiredEquity { get; set; }
        public double projectDuration { get; set; }
        public decimal interestRate { get; set; }

        // timestamps
        [BsonIgnoreIfNull]    
        public DateTime dateCreated { get; set; }
        public DateTime dateModified { get; set; }

        // costs
        public ICollection<Cost> costs { get; set; }

        public IList<CashFlowMonth> cashFlow { get; set; }

        public IList<ProposalTab> tabs { get; set; }

        public IList<FileTag> files { get; set; }
        
        public IEnumerable<Cost> hardCosts() { return costs.Where(a => a.isHard()); }
        public static ICollection<Cost> buildDefaultCosts()
        {
            var costs = new Collection<Cost> 
            {
                // hard costs: at least ONE
                new Cost { name = "residential", groupName = "hard", buildableArea = new BuildableArea { area = 2000 } }, 
                new Cost { name = "commercial", groupName = "hard", buildableArea = new BuildableArea { area = 100 }, isOptional = true},
                new Cost { name = "parking", groupName = "hard", buildableArea = new BuildableArea { area = 100 }, isOptional = true},

                // soft costs: 0 or MANY
                new Cost { groupName = "soft", name = "whatever" }, 
                new Cost { groupName = "soft", name = "whatever2", isOptional = true, isOptionalActive = true },

                // land costs: only "landprice" MUST be there, 0 or MANY more
                new Cost { groupName = "land", name = "price", land = new Land { address = "wewefe" } }, 
                new Cost { groupName = "land", name = "price", land = new Land { } }, 
                new Cost { groupName = "land", name = "whatever" },
                new Cost { groupName = "land", name = "whatever2", isOptional = true, isOptionalActive = false },

                // contingency: exact names, either there or not
                new Cost { groupName = "contingency", name = "soft" },
                new Cost { groupName = "contingency", name = "hard" },

                // management: exact names, either there or not
                new Cost { groupName = "management", name = "development" },
                new Cost { groupName = "management", name = "construction" },
                new Cost { groupName = "management", name = "marketing" },
            };

            return costs;
        }
    };
}
