﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using Zuilding.Web.Repository;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Zuilding.Web.Domain
{
    public class FileTag: ITenantEntity
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string id { get; set; }
        public string fileName { get; set; }
        public string type { get; set; }
        [BsonIgnoreIfNull]
        public string fileId { get; set; } // id used to retrieve the actual file 
        public string description { get; set; }
        public bool isInternal { get; set; }
        public string uploaderId { get; set; }
    }
}
