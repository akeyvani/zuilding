﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Zuilding.Web.Repository;

namespace Zuilding.Web.Domain.Checklist
{
    public class Project: ITenantEntity
    {
        // key
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string id { get; set; }

        public string name { get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime dateModified { get; set; }
    }
}