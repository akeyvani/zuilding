﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zuilding.Web.Repository;

namespace Zuilding.Web.Domain
{
    public class BuilderConfig: ITenantEntity
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string id { get; set; }

    }
}
